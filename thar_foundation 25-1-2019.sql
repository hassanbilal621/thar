/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : thar_foundation

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-25 22:09:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` timestamp NULL DEFAULT NULL,
  `staff_role` varchar(255) DEFAULT NULL,
  `assigned_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('2', 'Hassan Jahangirss', 'admin', '$2y$10$2UOlfn6u8P.Gu7y52sMh3OFtICgFhcd1dNovIOheLu2/IetdylQf6', null, null, null);

-- ----------------------------
-- Table structure for assign
-- ----------------------------
DROP TABLE IF EXISTS `assign`;
CREATE TABLE `assign` (
  `assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `assign_user_id` int(11) DEFAULT NULL,
  `assign_project_id` int(11) DEFAULT NULL,
  `assign_date` datetime DEFAULT NULL,
  `assign_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`assign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of assign
-- ----------------------------
INSERT INTO `assign` VALUES ('1', '1', '1', '2020-01-16 04:02:35', 'pending');
INSERT INTO `assign` VALUES ('3', '2', '1', '2020-01-16 04:02:35', 'pending');
INSERT INTO `assign` VALUES ('4', '1', '2', '2020-01-16 04:02:35', 'pending');
INSERT INTO `assign` VALUES ('5', '3', '3', '2020-01-22 09:28:19', 'pending');
INSERT INTO `assign` VALUES ('6', '2', '3', '2020-01-22 09:28:39', 'pending');
INSERT INTO `assign` VALUES ('7', '1', '2', '2020-01-22 09:29:00', 'pending');
INSERT INTO `assign` VALUES ('8', '3', '2', '2020-01-22 09:29:07', 'pending');
INSERT INTO `assign` VALUES ('9', '2', '3', '2020-01-22 09:29:13', 'pending');
INSERT INTO `assign` VALUES ('10', '1', '3', '2020-01-22 09:30:43', 'pending');
INSERT INTO `assign` VALUES ('11', '2', '4', '2020-01-23 09:28:06', 'pending');

-- ----------------------------
-- Table structure for choice
-- ----------------------------
DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice` (
  `choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `choice_text` text DEFAULT NULL,
  `choice_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`choice_id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `choice_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of choice
-- ----------------------------

-- ----------------------------
-- Table structure for forms
-- ----------------------------
DROP TABLE IF EXISTS `forms`;
CREATE TABLE `forms` (
  `survey_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`survey_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of forms
-- ----------------------------
INSERT INTO `forms` VALUES ('Testing', null, '1');
INSERT INTO `forms` VALUES ('Servey', null, '2');

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) DEFAULT NULL,
  `project_type` varchar(255) DEFAULT NULL,
  `project_detail` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `project_img` varchar(255) DEFAULT NULL,
  `project_status` tinyint(255) DEFAULT NULL COMMENT '0',
  `status_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('3', 'keyboard', null, 'ok', 'this is discription', '2020-01-16 12:35:37', 'Screenshot_91.png', '0', '0000-00-00 00:00:00', '3', null);
INSERT INTO `projects` VALUES ('5', 'laptop', null, null, 'keyboard', '2020-01-23 12:00:49', 'pngtree-dashboard-glyph-black-icon-png-image_691533_(1).jpg', '1', '2020-01-25 12:27:14', '8', null);
INSERT INTO `projects` VALUES ('11', 'laptop', null, null, 'delll390', '2020-01-25 12:41:51', 'profile-icon-png-889.png', '1', null, null, null);
INSERT INTO `projects` VALUES ('12', 'medical', null, null, 'medican', '2020-01-25 12:42:31', 'WhatsApp_Image_2020-01-20_at_6_10_51_PM1.jpeg', '0', null, null, null);

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` text DEFAULT NULL,
  `question_text` text DEFAULT NULL,
  `project_id_for_question` int(255) DEFAULT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES ('1', 'servey', 'Reporting Frequency: Weekly and Monthly', '1');
INSERT INTO `question` VALUES ('2', 'servey', 'Number of Patients', '1');
INSERT INTO `question` VALUES ('3', 'servey', 'Registered for Treatment', '1');
INSERT INTO `question` VALUES ('4', 'servey', 'Clinic Wise (Marvi, Gorano, IK OPD)', '0');
INSERT INTO `question` VALUES ('5', 'servey', 'Gender Wise', '0');
INSERT INTO `question` VALUES ('6', 'servey', 'Adult – Pead Wise', '0');
INSERT INTO `question` VALUES ('7', 'servey', 'Beneficiary Area Wise (Block II – Gorano – Rest of Islamkot)', '2');
INSERT INTO `question` VALUES ('8', 'servey', 'Diseases Treated', '2');
INSERT INTO `question` VALUES ('9', 'servey', 'Lab Tests Conducted', '2');
INSERT INTO `question` VALUES ('10', 'servey', 'Age-Wise Data', '0');
INSERT INTO `question` VALUES ('11', 'servey', 'Mother’s Data', '0');
INSERT INTO `question` VALUES ('26', 'number', 'TATTSS ', '0');
INSERT INTO `question` VALUES ('27', 'asdasdasd', 'Pappusss', '0');
INSERT INTO `question` VALUES ('28', 'sdsds', 'sdsd', '0');
INSERT INTO `question` VALUES ('29', 'dfdfd', 'dsd', '1');
INSERT INTO `question` VALUES ('30', 'sasasasa', 'sasasasas', '1');
INSERT INTO `question` VALUES ('31', 'ASAS', 'asa', '4');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `gender` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('2', 'abc', 'sdsdsdsdsd', 'admin', '123213', 'abc@gmail.com', '$2y$10$I4gS3NzPFHLj6/wHCHeOgeiONcchHrnmu82sNgZFA9cTA.OyJ.spe', '0000-00-00 00:00:00', '1', '42233123', '1', null);
INSERT INTO `staff` VALUES ('3', 'asdasd', 'sdsdsdsdsds', 'admin', '123123123', 'test@gmail.com', '$2y$10$kw2GAqIshk65Sjeczjf9puBMRVAqIykVraDnBI4YtFEd7U/3VSTRO', '0000-00-00 00:00:00', '1', '123123123123', '1', null);
INSERT INTO `staff` VALUES ('4', 'shehry', '', '', '03123456789', 'test20@gmail.com', '$2y$10$jQd9Cy4W7gwB6g80OEnSKeSItip6/.hM/mPPJGqTsF0sV8uYBMvLi', '2020-01-23 12:40:59', '2', '4230135296875', '3', null);
INSERT INTO `staff` VALUES ('5', 'shehry1', '', '', '03123456789', 'test55@gmail.com', '$2y$10$DGxkZbPUL/j3L2JGisfRT.UI6cqa4fkyAJjRlB1sw4EbCEpjH4Kka', '2020-01-23 12:40:55', '2', '4230135296875', '3', null);
INSERT INTO `staff` VALUES ('6', 'shehry12', '', '', '03123456789', 'test11@gmail.com', '$2y$10$mEoJyZ64bBrR3kEPIgkmmOiXIQypwVi3n1w.t1PnqR/FNwBEJ1vy2', '2020-01-23 12:40:46', '1', '4230135296875', '3', null);
INSERT INTO `staff` VALUES ('7', 'prince DJ', 'Shehry', 'princedjshehry', '03123456789', 'shehroz9712@gmail.com', '$2y$10$ngjUa8jXPBdQaHV83TBnmuhCCnvvF83b4hiOIMp0Azc3fQMbTQygS', '2020-01-23 12:40:57', '1', '4230135296875', '3', null);
INSERT INTO `staff` VALUES ('8', 'alyas', 'siddiqu', 'alyaz', '032425', 'alyaz@gmail.com', '$2y$10$IXJWXAFx/MVD33rydC0XS.7BZfp7vPKMPkS5ideM7Ja3S6NDask9q', '2020-02-15 00:00:00', '1', '4230135296875', '2', null);
INSERT INTO `staff` VALUES ('9', 'shehry', 'Shehry', 'hassain', '0324', 'email@gmail.com', '$2y$10$TzS44Zh3D4d7y4hUpAZoTeLyUgP88vLZC6z0TZ0Cxq3LQu/LGb2Ui', '2020-12-15 00:00:00', '1', '42301', '2', null);

-- ----------------------------
-- Table structure for survey
-- ----------------------------
DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_name` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of survey
-- ----------------------------

-- ----------------------------
-- Table structure for survey_answer
-- ----------------------------
DROP TABLE IF EXISTS `survey_answer`;
CREATE TABLE `survey_answer` (
  `survey_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_response_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_value` text NOT NULL,
  PRIMARY KEY (`survey_answer_id`),
  KEY `survey_response_id` (`survey_response_id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of survey_answer
-- ----------------------------
INSERT INTO `survey_answer` VALUES ('3', '4', '1', 'blabla');
INSERT INTO `survey_answer` VALUES ('4', '5', '2', 'clacla');
INSERT INTO `survey_answer` VALUES ('5', '6', '3', 'saddasd');
INSERT INTO `survey_answer` VALUES ('6', '7', '4', 'dasdasdas');
INSERT INTO `survey_answer` VALUES ('7', '8', '5', 'asdasdasd');
INSERT INTO `survey_answer` VALUES ('8', '9', '6', 'dadad');
INSERT INTO `survey_answer` VALUES ('9', '10', '7', 'addad');
INSERT INTO `survey_answer` VALUES ('10', '11', '8', 'asdad');
INSERT INTO `survey_answer` VALUES ('11', '12', '9', 'asda');
INSERT INTO `survey_answer` VALUES ('12', '13', '10', 'dasdasd');
INSERT INTO `survey_answer` VALUES ('13', '14', '11', 'adad');
INSERT INTO `survey_answer` VALUES ('14', '15', '12', 'asda');

-- ----------------------------
-- Table structure for survey_response
-- ----------------------------
DROP TABLE IF EXISTS `survey_response`;
CREATE TABLE `survey_response` (
  `survey_response_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id_for_survey` int(11) DEFAULT NULL,
  `time_taken` text DEFAULT NULL,
  `userid_for_survey` int(11) DEFAULT NULL,
  PRIMARY KEY (`survey_response_id`),
  KEY `survey_id` (`project_id_for_survey`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of survey_response
-- ----------------------------
INSERT INTO `survey_response` VALUES ('4', '3', '25-1-2020', '8');
INSERT INTO `survey_response` VALUES ('5', '5', '25-1-2020', '2');
INSERT INTO `survey_response` VALUES ('6', '11', '25-1-2020', '3');
INSERT INTO `survey_response` VALUES ('7', '12', '25-1-2020', '4');
INSERT INTO `survey_response` VALUES ('8', '3', '25-1-2020', '5');
INSERT INTO `survey_response` VALUES ('9', '5', '25-1-2020', '6');
INSERT INTO `survey_response` VALUES ('10', '11', '25-1-2020', '8');
INSERT INTO `survey_response` VALUES ('11', '12', '25-1-2020', '8');
INSERT INTO `survey_response` VALUES ('12', '3', '25-1-2020', '8');
INSERT INTO `survey_response` VALUES ('13', '5', '25-1-2020', '5');
INSERT INTO `survey_response` VALUES ('14', '11', '25-1-2020', '3');
INSERT INTO `survey_response` VALUES ('15', '12', '25-1-2020', '4');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `picture` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cv_attach` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'cba', '', '123213', 'abc@gmail.com', '', '$2y$10$I4gS3NzPFHLj6/wHCHeOgeiONcchHrnmu82sNgZFA9cTA.OyJ.spe', '2020-01-25 16:11:26', null, 'activate', null, null, '42233123', 'pending');
INSERT INTO `users` VALUES ('3', 'asdasd', '', '420420402', 'test@gmail.com', '', '$2y$10$kw2GAqIshk65Sjeczjf9puBMRVAqIykVraDnBI4YtFEd7U/3VSTRO', '2020-01-25 16:11:29', null, 'disable', null, null, '123123123123', 'pending');
INSERT INTO `users` VALUES ('4', 'shehry', '', '03123456789', 'test20@gmail.com', '', '$2y$10$jQd9Cy4W7gwB6g80OEnSKeSItip6/.hM/mPPJGqTsF0sV8uYBMvLi', '2020-01-25 16:11:33', null, null, null, null, '4230135296875', 'pending');
INSERT INTO `users` VALUES ('5', 'shehry', '', '03123456789', 'test55@gmail.com', '', '$2y$10$DGxkZbPUL/j3L2JGisfRT.UI6cqa4fkyAJjRlB1sw4EbCEpjH4Kka', '2020-01-25 16:02:28', null, null, null, null, '4230135296875', 'active');
INSERT INTO `users` VALUES ('6', 'shehry', '', '03123456789', 'test11@gmail.com', '', '$2y$10$mEoJyZ64bBrR3kEPIgkmmOiXIQypwVi3n1w.t1PnqR/FNwBEJ1vy2', '2020-01-25 16:05:03', null, null, null, null, '4230135296875', 'active');
INSERT INTO `users` VALUES ('8', 'sdsd', '', '45454544', 'sdsds@gmail.com', '', '$2y$10$.46zFlmg1zVCAHn7y3/ypuVEBDehZj51g2obimaW0PJGCQ2NKewIu', '2020-01-25 16:04:58', null, null, null, null, '445444', 'active');
