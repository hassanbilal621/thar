<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<div id="container">
	<div class="row">
		<div class="col s12">
			<div class="container">
				<div class="section">
					<div class="card">
						<div class="card-content">
							<div class="col s12">
								<h5 class="breadcrumbs-title mt-0 mb-0">TF Dashboard Graphsss</h5>
							</div>


							<?php echo form_open('dashboard/projectview/'.$projectid); ?>
							<!-- <div class="input-field col s4">
								<select class="browser-default" name="project" required>
									<?php foreach($projects as $project): 
										if( $project['project_id'] == $this->input->post('project')){
										
										  ?>
									<option value="<?php echo  $project['project_id']; ?>" selected><?php echo  $project['project_name']; ?></option>
									<?php
										} else {
										
										?>
									<option value="<?php echo  $project['project_id']; ?>"><?php echo  $project['project_name']; ?></option>
									<?php
										}
										endforeach; ?>
								</select>
							</div> -->
							<div class="input-field col s2">
								<input type="text" class="datepicker" name="starttime" placeholder="Start Time" value="<?php echo $this->input->post('starttime') ?>" required>
							</div>
							<div class="input-field col s2">
								<input type="text" class="datepicker" name="endtime" placeholder="End Time" value="<?php echo $this->input->post('endtime') ?>"  required>
							</div>
							<div class="input-field col s2">
								<input type="text" id="config-demo" class="form-control">
							</div>
							
							<div class="input-field col s4">
							<div class="input-field col s6">


								<a href="<?php echo base_url(); ?>dashboard/" class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2" type="submit" name="action">Back to Projects
									<i class="material-icons right">send</i>
								</a> 
						                           
								</div>
								<div class="input-field col s6">
								<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 " type="submit" name="action">Submit
							<i class="material-icons right">send</i>
							</button>   
							</div>
							</div>

						
							<?php echo form_close(); ?>
							<canvas id="bar-chart" width="700" height="300"></canvas>
						</div>
					</div>
				</div>
				<!-- START RIGHT SIDEBAR NAV -->
			</div>
		</div>
	</div>
</div>
<!-- END: Page Main-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>
	// Bar chart
	
	
	  var projects =  [];
	  <?php foreach($questions as $question): ?>
	  projects.push("<?php echo $question['question_text']?>");
	  <?php endforeach; ?>
	
	 console.log(projects);
	  new Chart(document.getElementById("bar-chart"), {
	      type: 'bar',
	      data: {
	        labels: projects,
	        datasets: [
	          {
	         
	            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
	            data: [2478,5267,734,784,433]
	          }
	        ]
	      },
	      options: {
	        legend: { display: false },
	        title: {
	          display: true,
	          text: 'Charts Showing According to Collected Data'
	        }
	      }
	  });
	  
</script>
<script>
	$(document).ready(function(){
	  $('.datepicker').datepicker();
	});
	        
	
</script>

<script>
$('#demo').daterangepicker({
    "showWeekNumbers": true,
    "startDate": "02/07/2020",
    "endDate": "02/20/2020"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
</script>