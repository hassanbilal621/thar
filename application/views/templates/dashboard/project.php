<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/date/daterangepicker.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/date/daterangepicker.js"></script>
<div id="container" style="height: 85%;">
	<div class="row">
		<div class="col s12">
			<div class="container">
				<div class="section">
					<div class="col s12">
						<div class="card">
							<div class="card-content">
								<h5 class="breadcrumbs-title mt-0 mb-0">TF Dashboard Graph</h5>
								<!-- <a href="<?php echo base_url(); ?>dashboard/" class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2" style="float:right;" name="action">Back to Projects
									<i class="material-icons right">send</i>
								</a> -->
								<?php echo form_open('dashboard/projectview/' . $projectid); ?>
								<div class="input-field col l6 s12">
									<input type="text" name="timeselection" id="demo" class="form-control">
								</div>
								<div class="input-field col l6 s12 left">
									<div class="input-field col s12">
										<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 " type="submit" name="action">Submit
											<i class="material-icons right">send</i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
		<!-- Form with placeholder -->
		<div class="row">
			<?php $ques = 0;
			?>
			<?php
			if (isset($questions)) {
				foreach ($questions as $question) : ?>
					<div class="col s12">
						<div class="row card">
							<div class="card-content">
								<?php if ($question['question_type'] == "int") { ?>
									<label for="name"> <?php echo $question['question_text']; ?></label>
									<div class="mb-1 col s12">
										<input type="Number" name="<?php echo $question['question_id']; ?>" class="validate valid" placeholder="Type in Numbers">
									</div>
								<?php
								} elseif ($question['question_type'] == "image") { ?>
									<div class="row">
										<div class="mb-1 col s12">
											<label for="name"> <?php echo $question['question_text']; ?></label>
										</div>
										<div class=" col s12">
											<input name='files[]' type="file" multiple>
										</div>
									</div>
								<?php
								} elseif ($question['question_type'] == "dropdown") { ?>
									<div class="row">
										<label for="name"> <?php echo $question['question_text']; ?></label>
										<div class="mb-1 col s12">
											<select class="browser-default" name="<?php echo $question['question_id']; ?> ">
												<option value="" disabled selected>Choose your option</option>
												<?php foreach ($question_dropdowns as $question_dropdown) :
													if ($question_dropdown['question_id'] == $question['question_id']) {  ?>
														<option value="<?php echo $question_dropdown['question_dropdown_id']; ?>"><?php echo $question_dropdown['question_dropdown']; ?></option>
												<?php }
												endforeach; ?>
											</select>
										</div>
									</div>
								<?php
								} elseif ($question['question_type'] == "options") { ?>
									<label for="name"> <?php echo $question['question_text']; ?></label>
									<div class="mb-1 col s12 card">
										<div class="card-content">
											<?php foreach ($question_dropdowns as $question_dropdown) :
												if ($question_dropdown['question_id'] == $question['question_id']) {  ?>
													<div class="row">
														<div class="col s4">
															<label for="name"> <?php echo $question_dropdown['question_dropdown']; ?></label>
														</div>
														<div class="col s6">
															<input name="<?php echo $question['question_id']; ?>options<?php echo $question_dropdown['question_dropdown_id']; ?>" type="Number" class="validate valid" placeholder="Type in Numbers">
														</div>
													</div>
											<?php }
											endforeach; ?>
										</div>
									</div>
								<?php
								} elseif ($question['question_type'] == "freq") { ?>
									<label for="name"> <?php echo $question['question_text']; ?></label>
									<div class="mb-1 col s12">
										<input name="<?php echo $question['question_id']; ?>" type="date" placeholder="Type in Alphabet">
									</div>
								<?php
								} else { ?>
									<label for="name"> <?php echo $question['question_text']; ?></label>
									<div class="mb-1 col s12">
										<input name="<?php echo $question['question_id']; ?>" type="text" placeholder="Type in Alphabet">
									</div>
								<?php
								} ?>
							</div>
						</div>
					</div>
				<?php
					$ques++;
				endforeach;


				?>
				<input type="hidden" name="project_id_user" value="<?php echo $projectid; ?>" />
		</div>
		<div class="input-field col s12">
			<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 right" type="submit" name="action">Submit
				<i class="material-icons right">send</i>
			</button>
		</div>
	<?php
			} else {
	?>
		<div class="card-content">
			<p class="caption center-align"><a>Alerts</a> There is no form assign to you <a href="<?php echo base_url(); ?>dashboard/">Back to Projects
				</a></p>
		</div>
	<?php
			}
	?>
	<!-- <canvas id="bar-chart" width="700" height="300"></canvas> -->
	</div>
</div>
</div>
<!-- START RIGHT SIDEBAR NAV -->
</div>
</div>
</div>
</div>
<!-- END: Page Main-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>
	// Bar chart
	new Chart(document.getElementById("bar-chart"), {
		type: 'bar',
		data: {
			labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
			datasets: [{
				label: "Population (millions)",
				backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
				data: [2478, 5267, 734, 784, 433]
			}]
		},
		options: {
			legend: {
				display: false
			},
			title: {
				display: true,
				text: 'Predicted world population (millions) in 2050'
			}
		}
	});
</script>
<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>
<script>
	$('#demo').daterangepicker({
		"showWeekNumbers": true,
		"startDate": "02/07/2020",
		"endDate": "02/20/2020"
	}, function(start, end, label) {
		console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});
</script>