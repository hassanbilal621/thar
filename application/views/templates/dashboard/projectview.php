<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/date/daterangepicker.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/date/daterangepicker.js"></script>

<script>
    function myFunction444() {
        alert("Page is loaded");
    }

    var gp = [];
</script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<div id="container">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">


              
          
                                <h5 class="breadcrumbs-title mt-0 mb-0" onload="myFunction444()">TF Dashboard Graph</h5>

                                <?php echo form_open('dashboard/projectview/' . $projectid); ?>
                                <div class="row mb-3">
                                    <div class="input-field col l6 s12">
                                        <input type="text" name="timeselection" id="demo" class="form-control">
                                    </div>
                                    <div class="input-field col l6 s12 left">
                                        <div class="input-field col s12">
                                            <button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 " type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col s12">
                                        <p class="caption center-align mb-1">if you want to back your project <a href="<?php echo base_url(); ?>dashboard/">click here
                                            </a></p>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                            <div class="row">
                                <div class="col s12">
                                    <div class="card-content">
                                        <div class="card-panel">

                                            <?php

                                            echo '
                                        <span class="blue-text text-darken-2"><h5 class="center-align">' . "Below Report Showing Data From " . $startdate . " to " . $enddate . '</h5>
                                        </span>';
                                            $CI = &get_instance();
                                            foreach ($questions as $question) :
                                                if ($question['question_type'] == "dropdown") {

                                                    foreach ($question_dropdowns as $dropdown) :
                                                        if ($question['question_id'] == $dropdown['question_id']) {


                                                            echo '
                                                                 <span class="blue-text text-darken-2"><h5 class="center-align">' . $dropdown['question_dropdown'] . '</h5>
                                                                 </span>';


                                                            foreach ($questions as $question1) :
                                                                if ($question1['question_type'] != "dropdown") {
                                                                    // echo $question1['question_id'];
                                                                    // echo "<br> ";

                                                                    if ($question1['question_type'] == "int") {



                                                                        $response_answer_value = 0;
                                                                        foreach ($responses as $response) {

                                                                            if ($CI->user_model->user_model->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])) {
                                                                                if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {

                                                                                    $reponse_answers = $CI->user_model->user_model->get_survey_answer($response['survey_response_id'], $question1['question_id']);
                                                                                    $response_answer_value = $response_answer_value + $reponse_answers['answer_value'];
                                                                                }
                                                                            }
                                                                        }

                                                                        // total number
                                                                        echo '<div class="row">
                                                                        <div class="col s8 l4">
                                                                            <label for="name"style="color: black !important;">' . $question1['question_text'] . ' : </label>
                                                                        </div>
                                                                        <div class="center-align  col s4 l2">
                                                                            <h6>' . $response_answer_value . '</h6>
                                                                        </div>
                                                                     </div>';

                                                                        //echo $response_answer_value;
                                                                    } elseif ($question1['question_type'] == "options") {

                                                                        echo '<div class="row">
									                                <div class="col s12">
									                                     <h6><label for="name" style="color: black !important;">' . $question1['question_text'] . ' : </label></h6>
									                               </div>';

                                                                        echo '<div class="mb-1 col s12">
                                                                         ';
                                                                         
                                                                         $dataPoints = array();

                                                                        foreach ($question_dropdowns as $questiondropdown) :
                                                                            if ($questiondropdown['question_id'] == $question1['question_id']) {
                                                                                // echo "<br> ";
                                                                                // echo "dropdown id" . $questiondropdown['question_dropdown_id'];
                                                                                // echo "<br> ";
                                                                                // echo "dropdown name" . $questiondropdown['question_dropdown'] . " = ";

                                                                                $response_answer_value1 = 0;
                                                                                foreach ($responses as $response) {

                                                                                    if ($CI->user_model->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])) {

                                                                                        if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {

                                                                                            $reponse_answers1 = $CI->user_model->user_model->get_survey_answer_option($response['survey_response_id'], $question1['question_id'], $questiondropdown['question_dropdown_id']);
                                                                                            $response_answer_value1 = $response_answer_value1 + $reponse_answers1['options_answer_value'];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                echo '   <div class="row">
                                                                                        <div class="col s8 l4">
                                                                                            <label for="name"> ' . $questiondropdown['question_dropdown'] . '</label>
                                                                                        </div>
                                                                                        <div class="center-align col s4 l2">
                                                                                            <h6>' . $response_answer_value1 . '</h6>
                                                                                        </div>
                                                                                    </div>';
                                                                                   
                                                                                    array_push($dataPoints, array('y'=>$response_answer_value1, 'label'=>str_replace(' ', '', $questiondropdown['question_dropdown'])));

                                                                            }
                                                                        endforeach;
                                                                        // echo '
                                                                        //     <script>

                                                                        //         var chart'.$dropdown['question_id'].'_'.$question1['question_id'].' = new CanvasJS.Chart("'.$dropdown['question_id'].'chartContainer'.$question1['question_id'].'", {
                                                                        //             animationEnabled: true,
                                                                        //             theme: "light2",
                                                                        //             title:{
                                                                        //                 text: "'. $question1['question_text'] .'"
                                                                        //             },
                                                                        //             axisY: {
                                                                        //                 title: "Numbers"
                                                                        //             },
                                                                        //             data: [{
                                                                        //                 type: "column",
                                                                        //                 yValueFormatString: "#,##0.## ",
                                                                          
                                                                        //                 dataPoints: '.json_encode($dataPoints, JSON_NUMERIC_CHECK).'
                                                                        //             }]
                                                                        //         });
                                                                        //         chart'.$dropdown['question_id'].'_'.$question1['question_id'].'.render();
                                                                                    

                                                                        //     </script>
                                                                        
                                                                        //     <div id="'.$dropdown['question_id'].'chartContainer'.$question1['question_id'].'" style="height: 370px; width: 100%;"></div>

                                                                    
                                                                        // ';
                                                                        echo '<div id="div'.$dropdown['question_dropdown_id'].'chart'.$question1['question_id'].'" style="height: 370px; width: 100%;"></div>';
                                                                        echo '<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2" id="'.$dropdown['question_dropdown_id'].'chart'.$question1['question_id'].'" onclick="loadGraph(this.id, this.value)" value='.json_encode($dataPoints, JSON_NUMERIC_CHECK).' >Generate '.$question1['question_text'].' Graph</button>';
                                                                       // echo '<div id="abc" style="height: 370px; width: 100%;"></div>';
                                                                   
                                                                    } elseif ($question1['question_type'] == "image") {

                                                                        echo '<div class="row">
                                                                        <div class="col s8 l4">
                                                                            <label for="name"style="color: black !important;">' . $question1['question_text'] . ' : </label>
                                                                        </div> 
                                                                        
                                                                        <div class="carousel">
                                                                        <a class="carousel-item" href="#one!"><img src="'.base_url().'assets/images/logo.png"></a>';
                                                                     


                                                                        foreach ($responses as $response) :
                                                                            if ($CI->user_model->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])) {

                                                                                if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {
                                                                        
                                                                               
                                                                                    foreach ($pictures as $picture) :
                                                                                        if ($picture['survey_response_id'] == $response['survey_response_id']  &&  $picture['survey_response_questionid'] == $question1['question_id']) {
                                                                                            echo '<a class="carousel-item" href="#one!"><img src="' . base_url() . 'assets/uploads/' .$picture['survey_response_picture']. '" /></a>';
                                                                                        }
                                                                                    endforeach;

                                                                                  
                                                                                }
                                                                            }
                                                                        endforeach;

                                                                        echo   '</div></div>';
                                                                    }
                                                                }
                                                            endforeach;

                                                            echo '</div>';
                                                        }


                                                    endforeach;
                                                }
                                            endforeach;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- START RIGHT SIDEBAR NAV -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

  
     <script>
       function loadGraph(buttonid, value) {
      
         
            console.log(JSON.parse(value));


            var chart = new CanvasJS.Chart("div"+buttonid, {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: ""
                },
                axisY: {
                    title: ""
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.##",
                    dataPoints: JSON.parse(value)
                }]
            });
            chart.render();
            
     }
     </script>
   

                                




         
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
        $('.carouselgds').carousel();
        $('.carousel').carousel();
        
    });
</script>
<script>
    $('#demo').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
