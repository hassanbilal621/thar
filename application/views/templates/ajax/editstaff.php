<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="col s12">
            <?php echo form_open('admin/updatestaff') ?>
            <div class="col s12">
               <h6 class="mt-5">Add Staff</h6>
               <div class="row">
                  <div class="input-field col s12">
                     <input type="hidden" value="<?php echo $staff['id']; ?>" name="staffid">
                     <input name="name" value="<?php echo $staff['name']; ?>" id="name2" type="text">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="fathername"value="<?php echo $staff['fathername']; ?>"  id="fname2" type="text">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="username" value="<?php echo $staff['username']; ?>" id="username2" type="text">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="email" value="<?php echo $staff['email']; ?>" id="email2" type="email">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="password" value="<?php echo $staff['password']; ?>" id="password2" type="password">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="phone" value="<?php echo $staff['phone']; ?>"  id="phone2" type="number">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="cnic" value="<?php echo $staff['cnic']; ?>"  id="cnic2" type="number">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <select name="gender" style="display: block;">
                        <option value="1">male</option>
                        <option value="2">female</option>
                     </select>
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <select name="designation" style="display: block;">
                        <option value="1">Admin</option>
                        <option value="2">Manager</option>
                        <option value="3">user</option>
                     </select>
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <input name="register_date" value="<?php echo $staff['register_date']; ?>"  id="register2" type="date">
                     <label for="register2">Register Date</label>
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <div class="input-field col s12">
                        <button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 mr-1 mb-2 box-shadow-none border-round right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <?php echo form_open() ?>
         </div>
      </div>
   </div>
</div>