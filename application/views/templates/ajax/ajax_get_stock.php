    <?php foreach($stocks as $stock): ?>
    
    <div class="col-lg-4">
        <div class="car-item gray-bg text-center">
            <div class="car-image">
                <img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/01.jpg" alt="">
                <div class="car-overlay-banner">
                <ul>
                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
                </div>
            </div>
            <div class="car-list">
                <ul class="list-inline">
                <li><i class="fa fa-registered"></i> <?php echo $stock['auction_date']; ?></li>
                <li><i class="fa fa-cog"></i> <?php echo $stock['maker']; ?> </li>
                <li><i class="fa fa-shopping-cart"></i> <?php echo $stock['engine_cc']; ?></li>
                </ul>
            </div>
            <div class="car-content">
                <!-- <div class="star">
                <i class="fa fa-star orange-color"></i>
                <i class="fa fa-star orange-color"></i>
                <i class="fa fa-star orange-color"></i>
                <i class="fa fa-star orange-color"></i>
                <i class="fa fa-star-o orange-color"></i>
                </div> -->
                <a href=""><?php echo $stock['model']; ?> </a>
                <div class="separator"></div>
                <div class="price">
                <span class="new-price"><?php echo $stock['sale_price']; ?> </span>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>