<div class="col s12 m12 l12">
    <div class="card-panel">
        <?php echo form_open('admin/manageproject'); ?>
        <h6>Assign Project</h6>
        <input type="hidden" name="projectid" value="<?php echo $project['project_id']; ?>" >
        <select class="browser-default " name="assignuser">
            <?php foreach ($users as $user) : ?>
                <option value="<?php echo $user['id']; ?>"><?php echo $user['name']; ?></option>
            <?php endforeach; ?>
        </select>
        <button class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-3 mr-1 mb-1 right" type="submit" name="action">submit
            <i class="material-icons left">send</i>
        </button>

      <?php echo form_close(); ?>
    </div>
</div>