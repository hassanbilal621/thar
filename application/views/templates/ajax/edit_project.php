<div class="row">
         <div class="col s12">
            <div class="card">
               <div class="col s12">
                  <?php echo form_open('admin/update_project') ?>
                  <!-- Form with placeholder -->
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Project Name*" name="project_name" value="<?php echo $project['project_name']; ?>" type="text">
                        <input type="hidden" value="<?php echo $project['project_id']; ?>" name="project_id" >
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Description*" name="description" value="<?php echo $project['description']; ?>" type="text">
               
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Status*" name="status" value="<?php echo $project['status']; ?>" type="text">
                     </div>
                  </div>
                 
                  <div class="row">
                     <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                        </button>
                     </div>
                  </div>
               </div>
               <?php echo form_open() ?>
            </div>
         </div>
      </div>