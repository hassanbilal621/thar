<div class="row">
         <div class="col s12">
            <div class="card">
               <div class="col s12">
                  <?php echo form_open('admin/update_survey') ?>
                  <!-- Form with placeholder -->
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Project Name*" name="survey_name" value="<?php echo $form['survey_name']; ?>" type="text">
                        <input type="hidden" value="<?php echo $form['survey_id']; ?>" name="survey_id" >
                     </div>
                  </div>
               
                  <div class="row">
                     <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                        </button>
                     </div>
                  </div>
               </div>
               <?php echo form_open() ?>
            </div>
         </div>
      </div>