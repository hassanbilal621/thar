
<div id="main">

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <h5>Edit User</h5>
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                        <div class="col s12">
                            <?php echo form_open('admin/updateuser') ?>
                            <!-- Form with placeholder -->
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="Name*" name="name" value="<?php echo $user['name']; ?>" type="text">
                                    <input type="hidden" value="<?php echo $user['id']; ?>" name="userid" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="Email*" name="email" value="<?php echo $user['email']; ?>" type="email">
                        
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="phone*" name="phone" value="<?php echo $user['phone']; ?>" type="phone">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="cnic*" name="cnic" value="<?php echo $user['cnic']; ?>" type="text">
                                </div>
                            </div>
                    
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 box-shadow-none border-round mr-1 mb-2 right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_open() ?>
                        </div>
                    </div>
                </div>

                <h5>Assign Projects</h5>

                
            </div>
        </div>
    </div>
</div>
</div>