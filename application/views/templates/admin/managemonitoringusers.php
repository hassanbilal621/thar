<div id="main">

   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
               <h4 class="card-title">Manage Monitoring User</h4>

                  <div class="row">
                  
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>User Id</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>CNIC</th>
                              <th>status</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($users as $user): ?>
                           <tr>
                              <td><?php echo $user['id']; ?></td>
                              <td><?php echo $user['name']; ?></td>
                              <td><?php echo $user['email'];?></td>
                              <td><?php echo $user['phone'];?></td>
                              <td><?php echo $user['cnic'];?></td>
                              <td><?php echo $user['status'];?></td>
                              <td> <a href="<?php echo base_url()."admin/editmonitoringuser/".$user['id']; ?>"  class=" box-shadow-none border-round waves-effect waves-light btn gradient-45deg-blue-indigo z-depth-2">Edit
                                 <i class="material-icons left">edit</i>
                                 </a>
                                 <a href="<?php echo base_url();?>admin/del_monitoring_user/<?php echo $user['id']?>" class=" box-shadow-none border-round waves-effect waves-light btn red z-depth-2" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>                       
                             </td>
                           </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


      function loaduserinfo(userid){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
               data:'country_name=pakistan',
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script>