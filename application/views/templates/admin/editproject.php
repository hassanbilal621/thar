<div id="main">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                
                <div class="box box-block bg-white">
                    <?php echo form_open_multipart('admin/update_project'); ?>
                    <div class="row">
                    <div class="col s6">
                              <input type="hidden" name="projectid" value="<?php echo $project['project_id']; ?>">
                              <table>
                                 <tr>
                                       <th>Project Name</th>
                                       <td><input type="text" name="project_name" value="<?php echo $project['project_name']; ?>"></td>
                                    </tr>
                                    <tr>
                                       <th>Project Description </th>
                                       <td><input type="text" name="description" value="<?php echo $project['description']; ?>"></td>
                                    </tr>
                                    <tr>
                                       <th>Project Status</th>
                                       <td><?php echo $project['status']; ?></td>

                                    </tr>
                                    <tr>
                                       <th>Assign User Name</th>
                                       <td><?php echo $project['name']; ?></td>
                                    </tr>
                                    <tr>
                                       <th>Assign Status</th>
                                       <td><?php echo $project['user_status']; ?></td>
                                    </tr>
                                   
                              </table>
                             
                           </div>
                           <div class="col s6">
                            <img id="view" src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>"" alt="Product Image" style="margin: 1px 0 0 201px;">
                            <input id="img2" type="file" name="userfile" onchange="previewimg(this);" accept="image/*" >
							    
                            </div>

                        </div>
                        <button class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-2 mr-1 mb-1 " type="submit" name="action">submit
                        <i class="material-icons right">mode_edit</i></button>
                        <?php echo form_close(); ?>
                        
                    <table>
                        <thead>
                            <tr>
                                <th>Questions</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($questions as $question) : ?>
                            <tr>
                                <td><?php echo $question['question_text']; ?></td>
                                <td><?php echo $question['question_type']; ?></td>
                                <td>
                                    <a class="waves-effect waves-light  btn red box-shadow-none border-round mr-1 mb-1" id="<?php echo $project['project_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
                                    <i class="material-icons left">delete_forever</i>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tfoot>
                    </table>
                           
                        <button class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-2 mr-1 mb-1  modal-trigger right"  href="#modal3" name="action">Add More Questions
                        <i class="material-icons left">add</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal3" class="modal">
	<div class="modal-content">
		<?php echo form_open('admin/addquestion'); ?>
		<h6>Add Question</h6>
		<input type="hidden" name="projectid" value='<?php echo $project['project_id']; ?>'>
		<div class="row">
			<div class="input-field col s12">
				<input name="question" type="text">
				<label for="title2">Question</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
                <select class="browser-default" name="type" onchange="checkDrodown(this.value) required">
                  
                    <option value="int">Number</option>
                    <option value="text">Text</option>
                    <option value="freq">Frequency</option>
                    <option value="dropdown">Dropdown</option>
                    <option value="dropdownvalue">Dropdown with Value</option>
                    <option value="options">Options Data</option>
                    <option value="image">Picture</option>
                </select>
                
			</div>
        </div>
        
        <div class="row">
			<div class="input-field col s12">
				
                <div id="dropdownoptions">
                    <table class="table table-bordered" id="dynamic_field"><tr><td><input type="text" name="name[]" placeholder="Enter Option" class="form-control name_list" /></td> <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  </tr> </table>
                </div> 
			</div>
		</div>

		<button class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-3 mr-1 mb-1  modal-trigger right" type="submit"  name="action">submit
		<i class="material-icons left">send</i>
		</button>
		<?php echo form_close();?>
	</div>
</div>

<script>

function checkDrodown(selectvalue){
    if(selectvalue == "dropdown" || selectvalue == "dropdownvalue" ){
        // document.getElementById("dropdownoptions").innerHTML = '';
      

    }else{
        // document.getElementById("dropdownoptions").innerHTML = "";
    }

}



</script>

<script>
      var i=1; 

      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="name[]" placeholder="Enter Option" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  

</script>


<script>
	$(document).ready(function(){
		  $("#showassign").click(function(){
		    $("#showtable").show();
		    $("#showassign").hide();
		  });
	    });
</script>
<script>
	var acc = document.getElementsByClassName("accordion");
	var i;
	
	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
	    this.classList.toggle("active");
	    var panel = this.nextElementSibling;
	    if (panel.style.display === "block") {
	      panel.style.display = "none";
	    } else {
	      panel.style.display = "block";
	    }
	  });
	}
</script>
<script>
function previewimg(input)
   {
         if (input.files && input.files[0]) 
         {
            var reader = new FileReader();

            reader.onload = function (e)
            {
               $('#view')
                     .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
         }
   }
</script>