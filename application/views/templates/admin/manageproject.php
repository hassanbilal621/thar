

<div id="main">

      <div class="row">
         <!-- Page Length Options -->
         <div class="row">
            <div class="col s12" style="mmin-height:700px;">
               <div class="card" style="min-height:700px;">
                  <div class="card-content">
                     <h5>Manage Product</h5>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>ID</th>
                              <th>Project Name</th>
                              <th>Actions</th>
                              <th>project Image</th>
                              <th>Project Description</th>
                              <th>Date/Time</th>
                              <th>Assign User</th>
                              <th>Project Status</th>
                              
                 
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($projects as $project) : ?>
                              <tr>
                                 <td><?php echo $project['project_id']; ?></td>
                                 <td><?php echo $project['project_name']; ?></td>
                                 <td>
                                    <a class='dropdown dropdown-trigger waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $project['project_id']; ?>'>Click Me</a>

                                    <ul id='dropdown1<?php echo $project['project_id']; ?>' class='dropdown-content'>
                                       <li>
                                          <a href="<?php echo base_url(); ?>admin/editproject/<?php echo $project['project_id']; ?>" type="submit" name="action" style="text-align: center;color: #ed981e;">Edit
                                             <i class="material-icons left" style="margin: 0px 0px 0px -15px;">edit</i>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="<?php echo base_url(); ?>admin/del_project/<?php echo $project['project_id']; ?>" name="action" style="text-align: center;color: #ed981e;">Delete
                                             <i class="material-icons left" style="margin: 0px 0px 0px -15px;">delete_forever</i>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="<?php echo base_url(); ?>admin/viewproject/<?php echo $project['project_id']; ?>" style="text-align: center;color: #ed981e;">View
                                             <i class="material-icons left" style="margin: 0px 0px 0px -15px;">visibility</i>
                                          </a>
                                       </li>
                                       <li>
                                       <?php if( $project['project_status']  == 0){ ?>
                                          <a href="<?php echo base_url(); ?>admin/completeproject/<?php echo $project['project_id']; ?>" style="text-align: center;color: #ed981e;margin: 0 0 0 20px;">Complete
                                             <i class="material-icons left" style="margin: -20px 0px 0px -34px;">done_all</i>
                                          </a>
                                          <?php }
                                          else{ ?>
                                          <a href="<?php echo base_url(); ?>admin/uncompleteproject/<?php echo $project['project_id']; ?>" style="text-align: center;color: #ed981e;margin: 0 0 0 20px;">Uncomplete
                                             <i class="material-icons left" style="margin: -20px 0px 0px -34px;">clear</i>
                                          </a>
                                          <?php } ?>
                                       </li>
                                       <li>
                                          <a id="<?php echo $project['project_id']; ?>"  onclick="loadprojectinfo(this.id)"  style="text-align: center;color: #ed981e;margin: 0 0 0 20px;" >Assign User
                                             <i class="material-icons left"style="margin: -15px 0px 0px -34px;">send</i>
                                          </a> 
                                       </li>
                                       <li>
                                          <a href="<?php echo base_url(); ?>admin/responceproject/<?php echo $project['project_id']; ?>" style="text-align: center;color: #ed981e;margin: 0 0 0 20px;" >Responce
                                             <i class="material-icons left"style="margin: -15px 0px 0px -34px;">send</i>
                                          </a> 
                                       </li>
                                    </ul>

                                 </td>
                                 <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>" width="80px"></td>
                                 <td><?php echo $project['description']; ?></td>
                                 <td><?php echo $project['date']; ?></td>
                                 <td><?php echo $project['name']; ?></td>
                                 <td><?php if ($project['project_status'] == 0 ){
                                    echo 'Uncomplete';
                                 }
                                 else{
                                    echo 'complete';
                                 } 
                                 ?></td>

                                 
                             
                              </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>   
<div id="modal3" class="modal">
   <div class="modal-content">
      
   </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
   function loadprojectinfo(project_id) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_assign_user_modal/"+project_id,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>