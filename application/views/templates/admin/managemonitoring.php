<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Manage Monitoring</h4>
                  
                                     
                                 
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>ID</th>
                              <th>Project Name</th>
                              <th>Project Type</th>
                              <th>Project Details</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($projects as $project ): ?>
                           <tr>
                              <td><?php echo $project['project_id']; ?></td>
                              <td><?php echo $project['project_name'];?></td>
                              <td><?php echo $project['project_type'];?></td>
                              <td><?php echo $project['project_detail'];?></td>
                              <td> <button class="userinfo btn waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 <button class="btn  waves-light red" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </button>                         
                             </td>
                           </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


      function loaduserinfo(userid){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
               data:'country_name=pakistan',
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script>