<style>
.modal .modal-content {
    padding: 15px !important;
}</style>
<div id="main">

	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
					<h4 class="card-title">Staff Users</h4>

						<div class="row">
							<div class="col s12">
								<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 box-shadow-none border-round mr-1 mb-2 modal-trigger right" href="#modal1">Add
									<i class="material-icons left">person_add</i>
								</button>
								<table id="page-length-option" class="display">
									<thead>
										<tr>
											<th>Staff Id</th>
											<th>Name</th>
											<th>Father Name</th>
											<th>User name</th>
											<th>gender</th>
											<th>Mobile</th>
											<th>Email</th>
											<th>Joining Date</th>
											<th>CNIC</th>
											<th>Designation</th>
											<th>Action</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($staffs as $staff) : ?>
											<tr>
												<td><?php echo $staff['id']; ?></td>											
												<td><?php echo $staff['name']; ?></td>
												<td><?php echo $staff['fathername']; ?></td>
												<td><?php echo $staff['username']; ?></td>
												<td><?php  
													if($staff['gender'] == 1)
														{
															echo "male";
														}	
													else
														{
															echo"female";
														}
													 ?>
												</td>

												<td><?php echo $staff['phone']; ?></td>
												<td><?php echo $staff['email']; ?></td>
												<td><?php echo $staff['register_date']; ?></td>
												<td><?php echo $staff['cnic']; ?></td>
												<td><?php  
													if($staff['designation'] == 1)
														{
															echo "admin";
														}	
													elseif($staff['designation'] == 2)
														{
															echo"manager";
														}
													else
														{
															echo"user";
														}
													 ?>
												</td>
												<td>
													<button id="<?php echo $staff['id']; ?>"  onclick="loaduserinfo(this.id)" class="waves-effect waves-light btn gradient-45deg-blue-indigo z-depth-2 box-shadow-none border-round">Edit
														<i class="material-icons left">edit</i>
													</button>
													<a href="<?php echo base_url();?>admin/del_staff/<?php echo $staff['id']?>" class="waves-effect waves-light btn red z-depth-2 box-shadow-none border-round" name="action">Delete
														<i class="material-icons left">delete_forever</i>
													</a>                       
												</td>
												<td></td>
											</tr>
										<?php endforeach; ?>
										</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal1" class="modal">
	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<?php echo form_open('admin/addstaff') ?>
					<div class="col s12">
						<h6 class="mt-5">Add Staff</h6>
						<div class="row">
							<div class="input-field col s12">
								<input name="name" id="name2" type="text">
								<label for="name2">Name</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="fathername" id="fname2" type="text">
								<label for="fname2">Father Name</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="username" id="username2" type="text">
								<label for="username2">User Name</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="email" id="email2" type="email">
								<label for="email2">Email</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="password" id="password2" type="password">
								<label for="password2">Password</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="phone" id="phone2" type="number">
								<label for="phone2">Phone</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input name="cnic" id="cnic2" type="number">
								<label for="cnic2">CNIC</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<select name="gender" class="browser-default" >
									<option value="1">Male</option>
									<option value="2">Female</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<select name="designation" class="browser-default" >
									<option value="1">Admin</option>
									<option value="2">Manager</option>
									<option value="3">User</option>
								</select>
							</div>
						</div>
						
						<div class="row">
							<div class="input-field col s12">
								<input type="date" class="datepicker" name="register_date" id="register_date" placeholder="mm/dd/yy">
								<label for="register_date">Register Date</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<div class="input-field col s12">
									<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 mr-1 mb-2 box-shadow-none border-round right" type="submit" name="action">Submit
									<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close('') ?>
				</div>
			</div>	
		</div>
	</div>
</div>

<div id="modal11" class="modal">
      <div class=" modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


      function loaduserinfo(staffid){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_staffmodal/"+staffid,
               data:'country_name=pakistan',
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script>