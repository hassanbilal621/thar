<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Add Monitoring</h4>
                  
                                          <?php echo form_open('admin/addproject') ?>
                                          <!-- Form with placeholder -->
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Project Name" name="project_name" type="text">
                                                <label for="name"> Project Name</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Project Type" name="project_type" type="text">
                                                <label for="type">Project Type</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Project Details" name="project_detail" type="text">
                                                <label for="detail">Project Details</label>
                                             </div>
                                          </div>
                                        
                                     
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                       
                                       <?php echo form_close() ?>
                                 
                
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <!-- <script type='text/javascript'>


      function loaduserinfo(userid){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
               data:'country_name=pakistan',
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script> -->