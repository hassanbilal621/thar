<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Manage Survey</h4>
                  
                                     
                                 
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>ID</th>
                              <th>Survey Name</th>
                              <th>User Id</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($forms as $form ): ?>
                           <tr>
                              <td><?php echo $form['survey_id']; ?></td>
                              <td><?php echo $form['survey_name'];?></td>
                              <td><?php echo $form['user_id'];?></td>
                              <td> <button id="<?php echo $form['survey_id']; ?>"  onclick="loaduserinfo(this.id)" class="userinfo btn waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 <a href="<?php echo base_url(); ?>admin/del_survey/<?php echo $form['survey_id']; ?>" class="btn  waves-light red" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>                         
                             </td>
                           </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


      function loaduserinfo(form_id){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_surveymodal/"+form_id,
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script>