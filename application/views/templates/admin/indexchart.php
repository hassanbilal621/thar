    <!-- BEGIN: Page Main-->
    <div id="main">
    <div class="row">

        <div class="col s12">
			<div class="container">
				<div class="section">
					<div class="card">
						<div class="card-content">
							<div class="col s12">
								<h5 class="breadcrumbs-title mt-0 mb-0">TF Dashboard Graph</h5>
							</div>

							<?php echo form_open('admin/index'); ?>
						   
							<div class="input-field col s4">
                              <select class="browser-default" name="project" required>

                                <?php foreach($projects as $project): 
                                  if( $project['project_id'] == $this->input->post('project')){

                                    ?>
                                
                                    <option value="<?php echo  $project['project_id']; ?>" selected><?php echo  $project['project_name']; ?></option>
                                    <?php

                                  } else {
                                  
                                  ?>
                                
                                <option value="<?php echo  $project['project_id']; ?>"><?php echo  $project['project_name']; ?></option>
                                <?php
                              
                              }
                              endforeach; ?>
                              </select>
                              
                           </div>
						
							<div class="input-field col s2">
								<input type="text" class="datepicker" name="starttime" placeholder="Start Time" value="<?php echo $this->input->post('starttime') ?>" required>
                           </div>
						      
							<div class="input-field col s2">
								<input type="text" class="datepicker" name="endtime" placeholder="End Time" value="<?php echo $this->input->post('endtime') ?>"  required>
                              
                           </div>
						   <div class="input-field col s2">
						   		<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 right" type="submit" name="action">Submit
									<i class="material-icons right">send</i>
								</button>                              
                           </div>
						   <?php echo form_close(); ?>
						   
						

							<canvas id="bar-chart" width="700" height="300"></canvas>

						</div>
					</div>
				</div><!-- START RIGHT SIDEBAR NAV -->

			</div>
        </div>
    </div>
    </div>
    <!-- END: Page Main-->



	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<script>
// Bar chart


  var projects =  [];
  <?php foreach($questions as $question): ?>
  projects.push("<?php echo $question['question_text']?>");
  <?php endforeach; ?>

 console.log(projects);
  new Chart(document.getElementById("bar-chart"), {
      type: 'bar',
      data: {
        labels: projects,
        datasets: [
          {
         
            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
            data: [2478,5267,734,784,433]
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: 'Charts Showing According to Collected Data'
        }
      }
  });
  </script>


  <script>
    $(document).ready(function(){
      $('.datepicker').datepicker();
    });
            

  </script>