<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Pending Stocks</h4>
                  <div class="row">
                     <div class="col s12">
          
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>Auction Date</th>
                                 <th>Auction</th>
                                 <th>Lot Number</th>
                                 <th>Maker</th>
                                 <th>Model</th>
                                 <th>City Location</th>
                                 <th>Actions</th>
                              </tr>
                           </thead>
                           <tbody><?php foreach($stocks as $stock): ?>
                              <tr>
                                 <td><?php
$date=date_create($stock['auction_date']);
echo date_format($date,"d / M / Y");
 ?></td>
                                 <td><?php echo $stock['auction'];?></td>
                                 <td><?php echo $stock['lot_number'];?></td>
                                 <td><?php echo $stock['maker'];?></td>
                                 <td><?php echo $stock['model'];?></td>
                                 <td><?php echo $stock['city_location'];?></td>
                                 <td>  
                                 <a href="<?php echo base_url(); ?>admin/activestatus/<?php echo $stock['stock_id'];?>" class="btn waves-effect waves-light green " name="action">Set Active
                                       <!-- <i class="material-icons left">edit</i> -->
                                       </a>
                                 <button class="btn waves-effect waves-light blue " type="submit" name="action">Edit
                                       <i class="material-icons left">edit</i>
                                       </button>
                                       <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
                                       <i class="material-icons left">delete_forever</i>
                                       </button>
                                 </td>
                              </tr>
                           <?php endforeach; ?>                  
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>