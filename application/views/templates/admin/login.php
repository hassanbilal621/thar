<style>
	::placeholder {
	color: #c79c6f;
	opacity: 1; /* Firefox */
	}
	:-ms-input-placeholder { /* Internet Explorer 10-11 */
	color: #c79c6f;
	}
	::-ms-input-placeholder { /* Microsoft Edge */
	color: #c79c6f ;
	}
</style>
<div class="row login-bg">
	<div class="col s12">
		<div class="container">
			<div id="login-page" class="row">
				<div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
					<?php echo form_open('admin/login'); ?>
					<div class="login-form">
						<div class="row">
							<div class="input-field col s12 center">
								<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" class="responsive-img valign">
								<?php if($this->session->flashdata('login_failed')): ?>
								<div id="card-alert" class="card gradient-45deg-amber-amber">
									<div class="card-content white-text">
										<p> <?php echo $this->session->flashdata('login_failed'); ?></p>
									</div>
									<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
									</button>
								</div>
								<?php endif; ?>
								<?php if($this->session->flashdata('user_loggedout')): ?>
								<div id="card-alert" class="card green">
									<div class="card-content white-text">
										<p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
									</div>
									<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
									</button>
								</div>
								<?php endif; ?>
								<p class="center login-form-text">Welcome Back! to thar Admin panel</p>
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12">
								<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/username.png" alt=""  class="prefix" style="padding: 8px 1px 0 0;margin: -8px 0 0 -11px;">
								<input id="username" name="username" type="text" placeholder="Username">
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12">
								<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/pass.png" alt=""  class="prefix" style="padding: 10px 0 0 0;margin: -5px 0 0 -10px;">
								<input id="password" name="password" type="password" placeholder="Password">
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button type="submit" name="login" class="waves-effect col s12 waves-light btn gradient-45deg-amber-amber z-depth-2">Login</button>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>