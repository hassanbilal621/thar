
<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">   
               <h4 class=" heading ">View Project</h4>
            </div>  
         </div>     
      </div> 
   </div>
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="box box-block bg-white">
                        <div class="row">
                           <div class="col s6">
                              
                              <table>
                                 <tr>
                                       <th>Project Name</th>
                                       <td><?php echo $project['project_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <th>Project Description</th>
                                       <td><?php echo $project['description']; ?></td>
                                    </tr>
                                    <tr>
                                       <th>Project Status</th>
                                       <td><?php echo $project['status']; ?></td>

                                    </tr>
                                    <tr>
                                       <th>Assign User Name</th>
                                       <td><?php echo $project['name']; ?></td>
                                    </tr>
                                    <tr>
                                       <th>Assign Status</th>
                                       <td><?php echo $project['user_status']; ?></td>
                                    </tr>
                                    
                              </table>
                             
                           </div>
                           <div class="col s6">
                           <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>"" alt="Product Image" style="margin: 1px 0 0 201px;">
                           </div>

                        </div>
                        
                           <table style="margin: 60px 0 0 0;">
                              <thead>
                                 <tr>
                                    <th>Questions</th>
                                    <th>Type</th>
                                 </tr>
                              </thead>
                              <tbody>
                              <?php foreach($questions as $question): ?>
                                 <tr>
                                    <td><?php echo $question['question_text']; ?></td>
                                    <td><?php echo $question['question_type']; ?></td>
                                 </tr>
                                 <?php endforeach;?>
                                 </tfoot>
                           </table>
                        
                         
                  </div>
                
                  <?php echo form_close()?>
                  <a class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mt-2 mb-2 mr-1 mb-1 right"  href="<?php echo base_url(); ?>admin/editproject/<?php echo $project['project_id']; ?>" type="submit" name="action">Edit This Project
							<i class="material-icons left">edit</i>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>