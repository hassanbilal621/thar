
<div id="main">

		<div class="row">

      
			<div class="col s12">
				<div class="card">
					<div class="card-content">
               <h5>Add Project</h5>
						<?php echo form_open_multipart('admin/addproject') ?>
						<!-- Form with placeholder -->
						<div class="row">
                     <div class="col s6">
                        <div class="row">
                           <div class="input-field col s12">
                              <input  name="project_name" type="text">
                              <label for="name"> Project Name</label>
                           </div>
                           
                           <div class="input-field col s12">
                              <input name="description" type="text">
                              <label for="description2">Description</label>
                           </div>
                           
                           <div class="input-field col s12">
                              <select class="browser-default" name="status">
                                 <option disabled selected value="">Select Project Status</option>
                                 <option value="0">Uncomplete</option>
                                 <option value="1">Complete</option>
                              </select>
                              
                           </div>
                        </div>
                     </div>
                     <div class="col s6">
                        <div class="row">
                        <div class=" fileupload input-field col s6 ">
										<h6 for="img2">Project Image</h6>
										<input id="img2" type="file" name="userfile" onchange="previewimg(this);" accept="image/*" >
										<img id="view" src="" alt="your image">
									</div>
                         
                        </div>
                     </div>
                  
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 right" type="submit" name="action">Submit
								<i class="material-icons right">send</i>
								</button>
							</div>
						</div>
						<?php echo form_close() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function previewimg(input)
   {
         if (input.files && input.files[0]) 
         {
            var reader = new FileReader();

            reader.onload = function (e)
            {
               $('#view')
                     .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
         }
   }
</script>