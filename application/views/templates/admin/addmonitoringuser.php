<div id="main">

	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">

					<h4 class="card-title">Add Monitoring User</h4>

						<div class="col s12">
							<?php echo form_open('admin/addmonitoringuser') ?>
							<!-- Form with placeholder -->
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="John Doe" name="name" type="text">
									<label for="name2">Name</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="john@domainname.com" name="email" type="email">
									<label for="email">Email</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="********" name="password" type="password">
									<label for="password">Password</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="00000-0000000-0" name="cnic" type="text">
									<label for="cnic2">CNIC</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input placeholder="+920000000000" name="phone" type="text">
									<label for="phone2">Phone</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 box-shadow-none border-round mr-1 mb-2 right" type="submit" name="action">Submit
									<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						</div>
						<?php echo form_open() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
