    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">

        <div class="col s12">
          <div class="container">
            <div class="section">
              <div class="card">
                <div class="card-content">
                  <div class="col s12">

                    <h5 class="breadcrumbs-title mt-0 mb-0">Select Project</h5>
                  </div>

                  <?php foreach ($projects as $project) : ?>
                    <div class="col s12 m2 center-align center">
                      <div class="card gradient-shadow border-radius-3 animate fadeUp">
                        <div class="card-content center">
                          <a href="<?php echo base_url(); ?>admin/projectview/<?php echo $project['project_id']; ?>">
                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>" class="width-40 z-depth-5" alt="">

                            <h5 class="lighten-4"><?php echo $project['project_name']; ?></h5>
                          </a>

                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>




                </div>
              </div>
            </div>
          </div>


          <!-- <canvas id="bar-chart" width="700" height="300"></canvas> -->

        </div>
      </div>
    </div><!-- START RIGHT SIDEBAR NAV -->


    <!-- END: Page Main-->