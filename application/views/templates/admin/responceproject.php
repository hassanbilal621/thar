<div id="main">
<div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">   
               <h4 class=" heading ">Responce Servay</h4>
            </div>  
         </div>     
      </div> 
   </div>
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<p class="card-title">Responce Servay</p>
						<div class="box box-block bg-white">
							<div class="row">
								<div class="col s6">
									<table>
										<tr>
											<th>Project Name</th>
											<td><?php echo $project['project_name']; ?></td>
										</tr>
										<tr>
											<th>Project Description</th>
											<td><?php echo $project['description']; ?></td>
										</tr>
										<tr>
											<th>Project Status</th>
											<td><?php if ($project['status'] == 0 ){
												echo 'Uncomplete';
												}
												else{
												echo 'complete';
												} 
												?></td>
										</tr>
									</table>
								</div>
								<div class="col s6">
									<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>"" alt="Product Image" style="margin: 0px 0px 0px 220px;width: 120px;">
								</div>
							</div>
							<table style="margin: 60px 0 0 0;">
								<thead>
									<tr>
										<th>Servay Id</th>
										<th>Date And Time</th>
										<th>User Name</th>
										<th>View</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($responces as $responce): ?>
									<tr>
										<td><?php echo $responce['survey_response_id']; ?></td>
										<td><?php echo $responce['time_taken']; ?></td>
										<td><?php echo $responce['name']; ?></td>
										<td>
											<a id="<?php echo $responce['survey_response_id']; ?>"  onclick="loadAnswerinfo(this.id)" class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round " type="submit" name="action">Answers
											<i class="material-icons left">edit</i>
											</a>
                              </td>
                              
									</tr>
									<?php endforeach;?>
                           </tfoot>
                     </table>
                     <h6><center style="margin: 20px 0 0 0;">No More Servay Responce</center></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal3" class="modal">
   <div class="modal-content">
      
   </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
   function loadAnswerinfo(servay_id) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_answer_modal/"+servay_id,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>