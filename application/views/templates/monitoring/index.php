<div id="container">

    <div id="ecommerce-offer">
      <div class="row">

      <div class="card">
						<div class="card-content">
							<div class="col s12">
								<h5 class="breadcrumbs-title mt-0 mb-0">Select Project</h5>
              </div>
              
              <?php foreach($projects as $project): ?>
                <div class="col s12 m2 center-align center">
                    <div class="card gradient-shadow border-radius-3 animate fadeUp">
                    <div class="card-content center">
                        <a href="<?php echo base_url(); ?>monitoring/projectview/<?php echo $project['project_id']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $project['project_img']; ?>" class="width-40 z-depth-5" alt="">
                        <h5 class="lighten-4"><?php echo $project['project_name']; ?></h5></a>

                    </div>
                    </div>
                </div>
              <?php endforeach; ?>

						</div>
					</div>

      


      </div>
    </div>
</div>
</div>