<div id="container">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Submit Monitoring Form</h4>
                    <?php echo form_open_multipart('monitoring/submitanswer') ?>
                    <!-- Form with placeholder -->
                    <div class="row">
                        <?php $ques = 0;
                        ?>
                        <?php
                        if (isset($questions)) {
                            foreach ($questions as $question) : ?>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="mb-1 col s12 card">
                                            <?php if ($question['question_type'] == "int") { ?>
                                                <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>
                                                <div class="mb-1 col s12">
                                                    <input type="number"    name="<?php echo $question['question_id']; ?>" class="validate valid" placeholder="Type in Numbers" required>
                                                </div>
                                            <?php
                                            } elseif ($question['question_type'] == "image") { ?>
                                                <div class="row">
                                                    <div class="mb-1 col s12">
                                                        <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>

                                                        <input name='files[]' type="file" multiple required>
                                                    </div>
                                                </div>
                                            <?php
                                            } elseif ($question['question_type'] == "dropdown") { ?>
                                                <div class="row">
                                                    <div class="mb-1 col s12">
                                                        <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>
                                                        <select class="browser-default" name="<?php echo $question['question_id']; ?>" required style="padding: 0!important;">
                                                            <option value="" disabled selected>Choose your option</option>
                                                            <?php foreach ($question_dropdowns as $question_dropdown) :
                                                                if ($question_dropdown['question_id'] == $question['question_id']) {  ?>
                                                                    <option value="<?php echo $question_dropdown['question_dropdown_id']; ?>"><?php echo $question_dropdown['question_dropdown']; ?></option>
                                                            <?php }
                                                            endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php
                                            } elseif ($question['question_type'] == "options") { ?>
                                                <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>
                                                <div class="">
                                                    <div class="card-content">
                                                        <?php foreach ($question_dropdowns as $question_dropdown) :
                                                            if ($question_dropdown['question_id'] == $question['question_id']) {  ?>
                                                                <div class="row">
                                                                    <div class="col s12">
                                                                        <h6 for="name" class="left"> <?php echo $question_dropdown['question_dropdown']; ?></h6>

                                                                        <input type="number" pattern="[1-9]" name="<?php echo $question['question_id']; ?>options<?php echo $question_dropdown['question_dropdown_id']; ?>" required class="validate valid" placeholder="Type in Numbers">
                                                                    </div>
                                                                </div>
                                                        <?php }
                                                        endforeach; ?>
                                                    </div>
                                                </div>
                                            <?php
                                            } elseif ($question['question_type'] == "freq") { ?>
                                                <div class="mb-1 col s12">
                                                    <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>
                                                    <select name="<?php echo $question['question_id']; ?>" class="browser-default">
                                                        <option value="1">Daily</option>
                                                        <option value="2">Weekly</option>
                                                        <option value="3">Monthly</option>
                                                    </select>
                                                    <!-- <input name="<?php echo $question['question_id']; ?>" type="date" placeholder="Type in Alphabet" required> -->
                                                </div>
                                            <?php
                                            } else { ?>
                                                <h6 class="mt-1" for="name"> <?php echo $question['question_text']; ?></h6>
                                                <div class="mb-1 col s12">
                                                    <input name="<?php echo $question['question_id']; ?>" type="text" placeholder="Type in Alphabet" required>
                                                </div>
                                            <?php
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                $ques++;
                            endforeach;


                            ?>
                            <input type="hidden" name="project_id_user" value="<?php echo $projectid; ?>" />
                    </div>
                    <div class="input-field col s12">
                        <button class="waves-effect waves-light btn gradient-45deg-amber-amber z-depth-2 right" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                <?php
                        } else {
                ?>
                    <div class="card-content">
                        <p class="caption"><a>Alerts</a> There is no form assign to you</p>
                    </div>
                <?php
                        }
                ?>
                <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>