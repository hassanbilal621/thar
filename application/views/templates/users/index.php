
<div id="container">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<center>
						<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
					</center>
				</div>

				<div class="col s12 m12 l6">
					<!-- Current Balance -->
					<div class="  animate fadeLeft">
						<div class=" card-content">
							<div class="row login-bg-1">
								<div id="login-page" class="row" style="height: 100%;">
									<div class="col s8 m6 l6 z-depth-5 card-panel border-radius-6 login-card bg-opacity-8">
										<?php echo form_open('monitoring/login'); ?>
										<div class="login-form">
											<div class="row">
												<div class="input-field col s12 center">
													<?php if($this->session->flashdata('login_failed')): ?>
													<div id="card-alert" class="card gradient-45deg-amber-amber">
														<div class="card-content white-text">
															<p> <?php echo $this->session->flashdata('login_failed'); ?></p>
														</div>
														<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">×</span>
														</button>
													</div>
													<?php endif; ?>
													<?php if($this->session->flashdata('user_loggedout')): ?>
													<div id="card-alert" class="card green">
														<div class="card-content white-text">
															<p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
														</div>
														<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">×</span>
														</button>
													</div>
													<?php endif; ?>
													<h5 class="center login-form-text">Reporting Dashboard</h5>
												</div>
											</div>
											<div class="row margin">
												<div class="input-field col s12">
													<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/username.png" alt=""  class="prefix" style="padding: 8px 1px 0 0;margin: -8px 0 0 -11px;">
													<input  name="username" type="text" placeholder="Username">
												</div>
											</div>
											<div class="row margin">
												<div class="input-field col s12">
													<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/pass.png" alt=""  class="prefix" style="padding: 10px 0 0 0;margin: -5px 0 0 -10px;">
													<input name="password" type="password" placeholder="Password">
												</div>
											</div>
											<div class="row">
												<div class="input-field col s12">
													<button type="submit" name="login" class="waves-effect col s12 waves-light btn gradient-45deg-amber-amber z-depth-2"">Login</button>
												</div>
											</div>
										</div>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6">
					<div class=" animate fadeRight">
						<div class="card-content">
							<div class="row login-bg-2">
								<div id="login-page" class="row" style="height: 100%;">
									<div class="col s8 m6 l6 z-depth-5 card-panel border-radius-6 login-card bg-opacity-8">
										<?php echo form_open('dashboard/login'); ?>
										<div class="login-form">
											<div class="row">
												<div class="input-field col s12 center">
													<?php if($this->session->flashdata('login_faileds')): ?>
													<div id="card-alert" class="card gradient-45deg-amber-amber">
														<div class="card-content white-text">
															<p> <?php echo $this->session->flashdata('login_faileds'); ?></p>
														</div>
														<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">×</span>
														</button>
													</div>
													<?php endif; ?>
													<?php if($this->session->flashdata('user_loggedouts')): ?>
													<div id="card-alert" class="card green">
														<div class="card-content white-text">
															<p><?php echo $this->session->flashdata('user_loggedouts'); ?></p>
														</div>
														<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">×</span>
														</button>
													</div>
													<?php endif; ?>
													<h5 class="center login-form-text">Monitoring Dashboard</h5>
												</div>
											</div>
											<div class="row margin">
												<div class="input-field col s12">
													<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/username.png" alt=""  class="prefix" style="padding: 8px 1px 0 0;margin: -8px 0 0 -11px;">
													<input  name="username" type="text" placeholder="Username">
												</div>
											</div>
											<div class="row margin">
												<div class="input-field col s12">
													<img src="<?php echo base_url(); ?>assets/app-assets/images/icon/pass.png" alt=""  class="prefix" style="padding: 10px 0 0 0;margin: -5px 0 0 -10px;">
													<input name="password" type="password" placeholder="Password">
												</div>
											</div>
											<div class="row">
												<div class="input-field col s12">
													<button type="submit" name="login" class="waves-effect col s12 waves-light btn gradient-45deg-amber-amber z-depth-2">Login</button>
												</div>
											</div>
										</div>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>