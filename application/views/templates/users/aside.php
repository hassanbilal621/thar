<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
	<!---/////////////////////////////////////////-------------logo-----------///////////////////////////////////////// -->
	
	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin/">			
			<img src="<?php echo base_url();?>assets/images/onlylogo.png"  alt=" dfdfdf" style="height: 35px;margin: -19px -70px 21px -5px;">
      <span class="logo-text hide-on-med-and-down"> <img src="<?php echo base_url();?>assets/images/text.png" style="height: 20px;margin: 0 0 -2px 0;" alt="Thar Foundation"></span>
			</a>
			<a class="navbar-toggler" href="#">
			<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">
        	
	 <li class="navigation-header"><a class="navigation-header-text"style="color: black;">Dashboard</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/dashboard.png" alt=""style="height: 30px;margin: 6px 18px 0px 0px;><span class="menu-title" data-i18n="">Home</span></a>
        </li>
      

      <li class="navigation-header"><a class="navigation-header-text"style="color: black;">Users</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/administrator"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/staff.png" alt=""style="height: 30px;margin:8px 29px -5px 0px;"><span class="menu-title" data-i18n="">Staff</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/users"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/clint.png" alt=""style="height: 30px;margin: 8px 20px -5px 0px;"><span class="menu-title" data-i18n="">Clients</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/pendingusers"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/panding.png" alt=""style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">Pending Users</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/activateusers"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/active.png" alt="" clas="gradient-45deg-amber-amber"style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">Activate Users</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/allusers"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/alluser.png" alt=""style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">All Users</span></a>
        </li>
        <li class="navigation-header"><a class="navigation-header-text"style="color: black;">Projects</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li> 

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/addproject"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/addproject.png" alt=""style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">Add Project</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manageproject"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/manageproject.png" alt=""style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">Manage Project</span></a>
        </li>
        
        <li class="navigation-header"><a class="navigation-header-text"style="color: black;">sign Out</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li> 
        
        <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/logout"><img src="<?php echo base_url();?>/assets/app-assets/images/icon/logout.png" alt=""style="height: 30px;margin: 8px 15px -5px 0px;"><span class="menu-title" data-i18n="">Sign Out</span></a></li>

      </ul>
 

      <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->