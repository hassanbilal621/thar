<?php
class user_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->database();
    }
	
    public function login($username, $password)
    {
  
      $this->db->where('username', $username);
      $result = $this->db->get('admin');
  
      if ($result->num_rows() == 1) {
        $hash = $result->row(0)->password;
  
        if (password_verify($password, $hash)) {
          return $result->row(0)->id;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    public function get_questions($projectid){
		
      $this->db->where('project_id_for_question', $projectid);
      $result = $this->db->get('question');
  
      return $result->result_array();
    }

    
    public function get_questions_dropdown($projectid){
		
      $result = $this->db->get('question_dropdown');
  
      return $result->result_array();
    }
  

    public function loginmonitor($username, $password)
    {
  
      $this->db->where('email', $username);
      $result = $this->db->get('users');
  
      if ($result->num_rows() == 1) {
        $hash = $result->row(0)->password;
  
        if (password_verify($password, $hash)) {
          return $result->row(0)->id;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    public function logindash($username, $password)
    {
  
      $this->db->where('email', $username);
      $result = $this->db->get('users');
  
      if ($result->num_rows() == 1) {
        $hash = $result->row(0)->password;
  
        if (password_verify($password, $hash)) {
          return $result->row(0)->id;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    public function get_response_answers(){

        $this->db->order_by('survey_answer.survey_answer_id', 'DESC');
        $this->db->join('survey_response', 'survey_answer.survey_response_id = survey_response.survey_response_id', 'left');
        // $this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
        // $this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
        // $this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');
    
        $query = $this->db->get('survey_answer');
        return $query->result_array();
    

    }



    public function check_project($user_id, $projectid)
    {
      
      $this->db->where('assign_project_id', $projectid);
      $this->db->where('assign_user_id', $user_id);

      $result = $this->db->get('assign_project');
  
      return $result->row_array();
    }


    public function get_assign_projects(){

      $this->db->order_by('assign_project.assign_id', 'DESC');
      $this->db->join('projects', 'projects.project_id = assign_project.assign_project_id', 'left');
      // $this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
      // $this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
      // $this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');
  
      $query = $this->db->get('assign_project');
      return $query->result_array();
  

  }


  public function get_assign_projects_dashboard(){

    $this->db->order_by('assign_project_dash.assign_id', 'DESC');
    $this->db->join('projects', 'projects.project_id = assign_project_dash.assign_project_id', 'left');
    // $this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
    // $this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
    // $this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

    $query = $this->db->get('assign_project_dash');
    return $query->result_array();


}


public function get_response($projectid){
		
  $this->db->where('project_id_for_survey', $projectid);
  $result = $this->db->get('survey_response');

  return $result->result_array();

}

public function get_response_pictures(){
		
  $result = $this->db->get('survey_pictures');

  return $result->result_array();

}

public function get_survey_answer($responseid, $qid){
		
  $this->db->where('survey_response_id', $responseid);
  $this->db->where('question_id', $qid);
  $result = $this->db->get('survey_answer');

  return $result->row_array();

}


public function get_survey_answers($res, $ques, $drop){
		
  $this->db->where('survey_response_id', $res);
  $this->db->where('question_id', $ques);
  $this->db->where('answer_value', $drop);
  $result = $this->db->get('survey_answer');

  return $result->row_array();

}



public function get_survey_answer_option($responseid, $qid, $dropid){
		
  $this->db->where('options_response_id', $responseid);
  $this->db->where('options_question_id', $qid);
  $this->db->where('options_dropdown_id',$dropid);
  $result = $this->db->get('options_answer');

  return $result->row_array();

}


public function get_survey_answer_picture($responseid, $qid, $dropid){
		
  $this->db->where('options_response_id', $responseid);
  $this->db->where('options_question_id', $qid);
  $this->db->where('options_dropdown_id',$dropid);
  $result = $this->db->get('options_answer');

  return $result->row_array();

}


function checkresponse($res, $ques, $drop){


  $survey_answers = $this->get_survey_answers($res, $ques, $drop);
  if(isset($survey_answers)){
    return true;
    
  }else{
    return false;
  }

}
  
}