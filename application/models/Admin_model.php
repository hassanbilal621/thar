<?php
class Admin_model extends CI_Model
{

	public function login($username, $password)
	{

		$this->db->where('username', $username);
		$result = $this->db->get('admin');

		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/////////////////////////////////////////////////////////////////////Zain
	public function get_questions($projectid){
		
		$this->db->where('project_id_for_question', $projectid);
		$result = $this->db->get('question');

		return $result->result_array();
	}

	public function get_servayinfo($servay_id){
	
		//$this->db->join('survey_answer', 'survey_answer.survey_response_id = survey_response.survey_response_id', 'left');
		$this->db->join('question', 'survey_answer.question_id = question.question_id', 'left');

		$this->db->where('survey_response_id', $servay_id);
		$result = $this->db->get('survey_answer');

		return $result->result_array();
	}
	
////////////////////////////////////////Get SIngle Project
	public function get_user_project($userid)
	{

		$this->db->where('user_id', $userid);
		$result = $this->db->get('projects');

		return $result->row_array();
	}


	public function get_responce($projectid){
	
		$this->db->join('projects', 'survey_response.project_id_for_survey = projects.project_id', 'left');
		$this->db->join('users', 'survey_response.userid_for_survey = users.id', 'left');


		$this->db->where('project_id_for_survey', $projectid);
		$result = $this->db->get('survey_response');

		return $result->result_array();
	}

	

	public function api_add_user($enc_password)
	{
		$data = array(
			'name' => $this->input->get('name'),
			'phone' => $this->input->get('phone'),
			'email' => $this->input->get('email'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s'),
			'status' => "pending",
			'cnic' => $this->input->get('cnic')
		);

		$this->security->xss_clean($data);
		$this->db->insert('users', $data);
	}


	public function api_fetch_projects($user_id)
	{
		// $this->db->where('project_id', $user_id);
		$result = $this->db->get('projects');

		return $result->row_array();
	}


	public function get_userinfo($user_id)
	{
		$this->db->where('id', $user_id);
		$result = $this->db->get('users');

		return $result->row_array();
	}
	
	public function get_staffinfo($staffid)
	{
		$this->db->where('id', $staffid);
		$result = $this->db->get('staff');

		return $result->row_array();
	}

	public function get_projectinfo($project_id)
	{
		$this->db->where('project_id', $project_id);
		$result = $this->db->get('projects');

		return $result->row_array();
	}

	public function get_surveyinfo($form_id)
	{
		$this->db->where('survey_id', $form_id);
		$result = $this->db->get('forms');

		return $result->row_array();
	}


	function insert($data)
	{
		$this->db->insert_batch('stocks', $data);
	}

	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');

		return $result->row_array();
	}

	public function get_bloggerinfo($blogger_id)
	{
		$this->db->where('id', $blogger_id);
		$result = $this->db->get('blogger');

		return $result->row_array();
	}


// Users Get / Add / Update / Delete 

	public function add_user($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'status' => "pending",
			'register_date' => date('Y-m-d H:i:s'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->insert('users', $data);
	}

	public function activeuser($userid)
	{	$data = array(
			'status' => 'active'
		);

		$this->db->where('id', $userid);
		$this->db->update('users', $data);
	}

	public function pendinguser($userid)
	{	$data = array(
			'status' => 'pending'
		);

		$this->db->where('id', $userid);
		$this->db->update('users', $data);
	}

	
	public function add_monitoring_user($enc_password){
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'status' => "pending",
			'register_date' => date('Y-m-d H:i:s'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->insert('monitoring_users', $data);
	}

	public function get_monitoring_users(){
		$query = $this->db->get('monitoring_users');
		return $query->result_array();
	}

	public function get_monitoring_user($user_id){

		$this->db->where('id', $user_id);
		$result = $this->db->get('monitoring_users');

		return $result->row_array();
	
	}

	public function update_monitoring_user(){
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('userid'));
		$this->db->update('monitoring_users', $data);
	}

	public function del_monitoring_user($userid)	{
		$this->db->where('id', $userid);
		$this->db->delete('monitoring_users');
	}

	public function add_dashboard_user($enc_password){
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'status' => "pending",
			'register_date' => date('Y-m-d H:i:s'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->insert('dashboard_users', $data);
	}

	public function get_dashboard_users(){
		$query = $this->db->get('dashboard_users');
		return $query->result_array();
	}

	public function get_dashboard_user($user_id){

		$this->db->where('id', $user_id);
		$result = $this->db->get('dashboard_users');

		return $result->row_array();
	
	}

	public function update_dashboard_user(){
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('userid'));
		$this->db->update('dashboard_users', $data);
	}

	public function del_dashboard_user($userid)	{
		$this->db->where('id', $userid);
		$this->db->delete('dashboard_users');
	}

//////// Users Get / Add / Update / Delete End

	public function addstaff($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'fathername' => $this->input->post('fathername'),
			'username' => $this->input->post('username'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'register_date' => $this->input->post('register_date'),
			'cnic' => $this->input->post('cnic'),
			'gender' => $this->input->post('gender'),
			'designation' => $this->input->post('designation')

		);

		$this->security->xss_clean($data);
		$this->db->insert('staff', $data);
	}

	public function get_staff()
	{

		$result = $this->db->get('staff');

		return $result->result_array();
	}

	public function updatestaff()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'fathername' => $this->input->post('fathername'),
			'username' => $this->input->post('username'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'register_date' => $this->input->post('register_date'),
			'cnic' => $this->input->post('cnic'),
			'gender' => $this->input->post('gender'),
			'designation' => $this->input->post('designation')


		);

		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('staffid'));
		$this->db->update('staff', $data);
	}


	public function updateuser()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'cnic' => $this->input->post('cnic')

		);

		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('userid'));
		$this->db->update('users', $data);
	}


	public function changepass_userpass($user_id, $password)
	{
		$data = array(
			'password' => $password
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);
	}

	public function update_user($user_id, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
			'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'picture' => $imgname
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);
	}


	public function del_staff($staffid)
	{
		$this->db->where('id', $staffid);
		$this->db->delete('staff');
	}
	
	public function del_user($userid)
	{
		$this->db->where('id', $userid);
		$this->db->delete('users');
	}

	// public function get_users(){
	//     $this->db->order_by('users.id', 'DESC');
	//     $query = $this->db->get('users');
	//     return $query->result_array();
	// }

	public function get_orders()
	{
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

		$query = $this->db->get('users_orders');
		return $query->result_array();
	}
	public function get_activestock()
	{
		$this->db->where('status', 'active');

		$this->db->order_by('stocks.stock_id', 'DESC');
		$query = $this->db->get('stocks');
		return $query->result_array();
	}


	public function get_pendingstock()
	{
		$this->db->where('status', 'pending');

		$this->db->order_by('stocks.stock_id', 'DESC');
		$query = $this->db->get('stocks');
		return $query->result_array();
	}


	public function get_expiredstocks()
	{
		$this->db->where('status', 'expired');

		$this->db->order_by('stocks.stock_id', 'DESC');
		$query = $this->db->get('stocks');
		return $query->result_array();
	}


	public function get_invoices()
	{
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

		$query = $this->db->get('users_orders');
		return $query->result_array();
	}

	public function get_countries()
	{
		$this->db->order_by('countries.country_name', 'ASC');
		$query = $this->db->get('countries');
		return $query->result_array();
	}

	public function get_cities($country_name)
	{
		$this->db->where('country_name', $country_name);
		$this->db->order_by('cities.city_name', 'ASC');
		$query = $this->db->get('cities');
		return $query->result_array();
	}

	public function get_bloggers()
	{
		$this->db->order_by('blogger.id', 'DESC');
		$query = $this->db->get('blogger');
		return $query->result_array();
	}

	public function get_admins()
	{
		$this->db->order_by('admin.id', 'DESC');
		$query = $this->db->get('admin');
		return $query->result_array();
	}

	public function add_admin($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
		);

		$this->security->xss_clean($data);
		$this->db->insert('admin', $data);
	}

	public function add_city()
	{
		$data = array(
			'geoname_id' => $this->input->post('geoname_id'),
			'country_iso_code' => $this->input->post('country_iso_code'),
			'country_name' => $this->input->post('country_name'),
			'time_zone' => $this->input->post('time_zone'),
			'city_name' => $this->input->post('city_name')
		);

		$this->security->xss_clean($data);
		$this->db->insert('cities', $data);
	}



	public function add_blogger($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
		);

		$this->security->xss_clean($data);
		$this->db->insert('blogger', $data);
	}

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);
	}


	public function activestatus($stockid)
	{
		$data = array(
			'status' => "active"
		);

		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stocks', $data);
	}


	public function update_blogger($blogger_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password
		);
		$this->security->xss_clean($data);
		$this->db->where('id', $blogger_id);
		$this->db->update('blogger', $data);
	}


	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}
	public function del_bloggers($bloggerid)
	{
		$this->db->where('id', $bloggerid);
		$this->db->delete('blogger');
	}
	public function del_city($city_name)
	{
		$this->db->where('city_name', $city_name);
		$this->db->delete('cities');
	}

	public function get_jobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	

	public function get_approvejobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	public function get_pendingjobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	public function get_appliedjobs()
	{
		$this->db->order_by('applications.appid', 'DESC');
		$this->db->join('users', 'applications.userid = users.id', 'left');
		$this->db->join('jobs', 'applications.jobid = jobs.jobid', 'left');
		$query = $this->db->get('applications');

		return $query->result_array();
	}

	public function get_users()
	{
		$query = $this->db->get('users');
		return $query->result_array();
	}


	public function get_active_users()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('users');
		
		return $query->result_array();
	}


	public function get_pending_users()
	{	
		$this->db->where('status', 'pending');
		$query = $this->db->get('users');
		
		return $query->result_array();
	}
	// Project Get / Add / Update / Delete 

	public function add_project($imgname)
	{
		$data = array(
			'project_name' => $this->input->post('project_name'),
			'description' => $this->input->post('description'),
			'date' => date('Y-m-d H:i:s'),
			'project_img' => $imgname,
			'project_status' => $this->input->post('status')

		);
		$this->security->xss_clean($data);
		$this->db->insert('projects', $data);
	}

	public function add_question($project_id)
	{
		$data = array(
			'question_text' => $this->input->post('question'),
			'question_type' => $this->input->post('type'),
			'project_id_for_question' => $project_id
		
		);
		$this->security->xss_clean($data);
		$this->db->insert('question', $data);
        $insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}

	public function assign_project()
	{
		$data = array(
			'assign_project_id' => $this->input->post('assign_project_id'),
			'assign_user_id' => $this->input->post('assign_user_id')
		);

		$this->security->xss_clean($data);
		$this->db->insert('assign_project', $data);
	}




	public function add_option($option, $questionid)
	{
		$data = array(
			'question_dropdown' => $option,
			'question_id' => $questionid
		);
		$this->security->xss_clean($data);
		$this->db->insert('question_dropdown', $data);
		
	}

	public function assign_user($projectid)
	{
		$data = array(
			'user_id' => $this->input->post('assignuser')
			
		);

		$this->security->xss_clean($data);
		$this->db->where('project_id', $projectid);
		$this->db->update('projects', $data);
	}

	public function update_project($imgname)
	{
		$data = array(
			'project_name' => $this->input->post('project_name'),
			'description' => $this->input->post('description'),
			'project_img' => $imgname
		);

		$this->security->xss_clean($data);
		$this->db->where('project_id', $this->input->post('projectid'));
		$this->db->update('projects', $data);
	}
	public function get_projects()
	{
		$this->db->join('users', 'projects.user_id = users.id', 'left');

		$query = $this->db->get('projects');

		return $query->result_array();
	}
	
	
	public function get_project($project_id)
	{
		$this->db->join('users', 'projects.user_id = users.id', 'left'); 

		$this->db->where('project_id', $project_id);
		$result = $this->db->get('projects');

		return $result->row_array();
	}
	public function get_assign($projectid)
	{
		$this->db->where('assign_project_id', $projectid);
		$result = $this->db->get('assign');

		return $result->result_array();
	}


	public function del_project($project_id)
	{
		$this->db->where('project_id', $project_id);
		$this->db->delete('projects');
	}
	public function completeproject($projectid)
	{	$data = array(
			'project_status' => '1',
			'status_date' => date('Y-m-d H:i:s')
		);

		$this->db->where('project_id', $projectid);
		$this->db->update('projects', $data);
	}

	public function uncompleteproject($projectid)
	{	$data = array(
			'project_status' => '0',
			'status_date' =>  'null'
		);

		$this->db->where('project_id', $projectid);
		$this->db->update('projects', $data);
	}


	public function deleteques($projectid)
	{
		$this->db->where('project_id_for_question', $projectid);
		$result = $this->db->get('question');
	}
	// Survey Get / Add / Update / Delete 

	public function add_survey()
	{
		$data = array(
			'survey_name' => $this->input->post('survey_name'),
			'user_id' => $this->input->post('user_id')


		);

		$this->security->xss_clean($data);
		$this->db->insert('forms', $data);
	}


	public function update_survey()
	{
		$data = array(
			'survey_name' => $this->input->post('survey_name')

		);

		$this->security->xss_clean($data);
		$this->db->where('survey_id', $this->input->post('survey_id'));
		$this->db->update('forms', $data);
	}

	public function get_survey()
	{

		$this->db->join('users', 'users.id = forms.user_id', 'left');

		$query = $this->db->get('forms');
		return $query->result_array();
	}


	public function del_survey($form_id)
	{
		$this->db->where('survey_id', $form_id);
		$this->db->delete('forms');
	}
}
