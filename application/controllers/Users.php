<?php
class Users extends CI_Controller{

	public function __construct()
    {
      parent::__construct();
	  $this->load->model('user_model');
    }
	
	public function index(){

		if($this->session->userdata('monitoring_thar_id')){
            redirect('monitoring/');
		}

        if($this->session->userdata('dashboard_thar_id')){
            redirect('dashboard/');
        }
        
        $this->load->view('templates/users/header.php');
        $this->load->view('templates/users/index.php');
     
    }

    public function error(){

        $this->load->view('templates/users/header.php');
        $this->load->view('templates/users/error.php');


    }

}
