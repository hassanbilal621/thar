<?php
class Monitoring extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function login()
    {
        if ($this->session->userdata('monitoring_thar_id')) {
            redirect('monitoring/');
        }

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {

            $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');

            redirect('users/index');
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user_id = $this->user_model->loginmonitor($username, $password);
            //$employee_id = $this->employee_model->login($username, $password);

            if ($user_id) {
                $user_data = array(
                    'monitoring_thar_id' => $user_id,
                    'username' => $username,
                    'necxo_logged_in' => true
                );
                $this->session->set_userdata($user_data);

                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('monitoring/');
            } else {
                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('users/index');
            }
        }
    }




    public function logout()
    {

        $this->session->unset_userdata('monitoring_thar_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('necxo_logged_in');

        redirect('users/');
    }

    public function projectview($projectid)
    {
        if (!$this->session->admindata('monitoring_thar_id')) {
            redirect('users/login');
        }

        if (!$this->user_model->check_project($this->session->admindata('monitoring_thar_id'), $projectid)) {

            redirect('monitoring/');
        }

        $data['title'] = "Monitoring Form";
        // echo $this->session->admindata('monitoring_thar_id');
        // $projects =  $this->admin_model->get_user_project($this->session->admindata('monitoring_thar_id'));

        // $data['projects'] = $this->user_model->get_assign_projects($this->session->admindata('monitoring_thar_id'));

        $data['projectid'] =  $projectid;
        $data['questions'] = $this->user_model->get_questions($projectid);
        $data['question_dropdowns'] = $this->user_model->get_questions_dropdown($projectid);

        if (isset($projects)) {

            // $projectid = $projects['project_id'];

            // echo $projectid;

        }

        // print_r($data);

        // die;

        $this->load->view('templates/users/header.php');
        $this->load->view('templates/monitoring/navbar.php');
        $this->load->view('templates/monitoring/project.php', $data);
        $this->load->view('templates/users/footer.php');
    }

    public function index()
    {
        if (!$this->session->admindata('monitoring_thar_id')) {
            redirect('users/login');
        }

        $data['title'] = "Monitoring Form";
        // echo $this->session->admindata('monitoring_thar_id');
        // $projects =  $this->admin_model->get_user_project($this->session->admindata('monitoring_thar_id'));

        $data['projects'] = $this->user_model->get_assign_projects($this->session->admindata('monitoring_thar_id'));

        // if(isset($projects)){

        //     $projectid = $projects['project_id'];

        //     // echo $projectid;
        //     $data['projectid'] =  $projectid;
        //     $data['questions'] = $this->admin_model->get_questions($projectid);


        // }

        // print_r($data);

        // die;

        $this->load->view('templates/users/header.php');
        $this->load->view('templates/monitoring/navbar.php');
        $this->load->view('templates/monitoring/index.php', $data);
        $this->load->view('templates/users/footer.php');
    }


    public function  submitanswer()
    {
        if (!$this->session->admindata('monitoring_thar_id')) {
            redirect('admin/login');
        }

        //    echo "<table>";
        //     foreach ($_POST as $key => $value) {
        //         echo "<tr>";
        //         echo "<td>";
        //         echo $key;
        //         echo "</td>";
        //         echo "<td>";
        //         echo $value;
        //         echo "</td>";
        //         echo "</tr>";
        //     }
        //     echo "</table>";

        $data['title'] = 'Sign Up';

        if (isset($_POST['project_id_user'])) {

            // echo $_POST['project_id_user'];
            $data['questions'] = $this->admin_model->get_questions($_POST['project_id_user']);
            // echo '<pre>';
            // print_r($data);
            // echo '<pre>';
            // die;

            $surveyresponseid = $this->submit_response($_POST['project_id_user'], $this->session->admindata('monitoring_thar_id'));
            $data['question_dropdowns'] = $this->user_model->get_questions_dropdown($_POST['project_id_user']);


            // echo "<table style='border:2px solid black;'>";

            foreach ($data['questions'] as $question) {

                // echo $question['question_text']."</br>";

                $questionid = $question['question_id'];
                $questiontype = $question['question_type'];

                if ($questiontype == "image") {

                    $data1 = [];
                    $count = count($_FILES['files']['name']);
                    for($i=0;$i<$count;$i++){
                      if(!empty($_FILES['files']['name'][$i])){
                        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'assets/uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size'] = '5000';
                        $config['file_name'] = $_FILES['files']['name'][$i];
                        $this->load->library('upload',$config); 
                        if($this->upload->do_upload('file')){
                          $uploadData = $this->upload->data();
                          $filename = $uploadData['file_name'];
                          $data1['totalFiles'][] = $filename;
                        }
                      }
                    }
                                     
                    foreach($data1['totalFiles'] as $carimages){
 
            
                        $this->images_uploading($question['question_id'], $carimages, $surveyresponseid);
                    }
                  
                 
                } elseif ($questiontype == "options") {

                    // $str1 = '13255options214455';
                    // preg_match('/options(.*)/', $str1, $option);
                    // preg_match('/(.*)options/', $str1, $option1);
                    // $option_id = $option[1].PHP_EOL;
                    // $question_id = $option1[1].PHP_EOL;
                    // echo $option_id;
                    // echo "<br>";
                    // echo $question_id;
                    foreach ($data['question_dropdowns'] as $question_dropdown) {

                        if ($question_dropdown['question_id'] == $question['question_id']) {

                            $getdropdownval = $question['question_id'] . "options" . $question_dropdown['question_dropdown_id'];

                            // echo $getdropdownval;

                            // echo '<br>';

                            $questionvalue1 = $_POST[$getdropdownval];

                            $this->submit_options_answer($question['question_id'], $questionvalue1, $question_dropdown['question_dropdown_id'], $surveyresponseid);
                        }
                    }
                } elseif ($questiontype == "dropdown" || $questiontype == "int" || $questiontype == "freq") {

   
                        $questionvalue = $_POST[$question['question_id']];

                }

                // echo $questionid."</br>";
                // echo $questionvalue."</br>";


                //    echo "<tr><th>Question</th><th>Answer</th>";

                //    echo "<tr><td>".$questionid."</td><td>".$questionvalue."</td>";



                if (isset($questionvalue)) {

                    $this->submit_answer($questionid, $questionvalue, $surveyresponseid);
                    $questionvalue = 0;
                }
            }

            $this->load->view('templates/users/header.php');
            $this->load->view('templates/monitoring/navbar.php');
            $this->load->view('templates/monitoring/success.php');
            // $this->load->view('templates/users/footer.php');

            // echo "/table>";


        }
    }

    public function images_uploading($questionid, $imagename, $surveyresponseid)
    {
        $data = array(
            'survey_response_questionid' => $questionid,
            'survey_response_picture' => $imagename,
            'survey_response_id' => $surveyresponseid

        );

        $this->security->xss_clean($data);
        $this->db->insert('survey_pictures', $data);
    }


    public function submit_options_answer($questionid, $questionvalue, $dropdownid, $surveyresponseid)
    {
        $data = array(
            'options_question_id' => $questionid,
            'options_dropdown_id' => $dropdownid,
            'options_answer_value' => $questionvalue,
            'options_response_id' => $surveyresponseid

        );

        $this->security->xss_clean($data);
        $this->db->insert('options_answer', $data);
    }



    public function submit_answer($questionid, $questionvalue, $surveyresponseid)
    {
        $data = array(
            'question_id' => $questionid,
            'answer_value' => $questionvalue,
            'survey_response_id' => $surveyresponseid

        );

        $this->security->xss_clean($data);
        $this->db->insert('survey_answer', $data);
    }





    public function submit_response($projectid, $userid)
    {
        $data = array(
            'project_id_for_survey' => $projectid,
            'userid_for_survey' => $userid,
            'time_taken' => date('Y-m-d H:i:s')

        );

        $this->security->xss_clean($data);
        $this->db->insert('survey_response', $data);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
}
