<?php
class admin extends CI_Controller{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin_model');
      $this->load->library('excel');
    }


    public function api_login_request(){
 
        $data['title'] = 'Nexco Japan';


        if(!isset($_GET['username']) && !isset($_GET['password'])){

            echo "fieldsinvalid";
            exit;
            
        }else{

            $username = $this->input->get('username');
            $password = $this->input->get('password');

            //$user_id = $this->user_model->login($username, $password);
            $thar_admin_id = $this->admin_model->login($username, $password);

            if($thar_admin_id){
                echo $thar_admin_id;
                exit;
			} 
	
            else {

                echo "incorrectpass";
                exit;
            }	

        }
  
    }
	
/* 	public function api_change_paassword_account(){
        if(){
		}
		

        if(!$this->form_validation->run() === FALSE){
         
            echo "fieldmissing";
            exit;

        }else{

            $enc_password = password_hash($this->input->get('password'), PASSWORD_DEFAULT);

            $this->admin_model->api_add_user($enc_password);
            echo "accountregister";
            exit;

        }
    
    }
 */


    public function api_register_account(){
        

        $this->form_validation->set_rules('name', 'FullName', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');

        if(!$this->form_validation->run() === FALSE){
         
            echo "fieldmissing";
            exit;

        }else{

            $enc_password = password_hash($this->input->get('password'), PASSWORD_DEFAULT);

            $this->admin_model->api_add_user($enc_password);
            echo "accountregister";
            exit;

        }
    
    }

    public function api_changepassword_user(){

    
        if(isset($_GET['userid'])){
			$enc_password = password_hash($_GET['password'], PASSWORD_DEFAULT);
            $this->admin_model->changepass_userpass($_GET['userid'], $enc_password);
       
            echo "successfull";
            exit;

        }else{
            echo "failed";
            exit;
        }
  
    }

    public function api_fetch_projects(){

    
        if(isset($_GET['a'])){

            $project = $this->admin_model->api_fetch_projects($_GET['userid']);
            $data['users'] = $project;
            echo json_encode($project);

        }else{
            echo "false";
            exit;
        }
  
    }





    public function api_fetch_userdetails(){

    
        if(isset($_GET['userid'])){

            $currUser = $this->admin_model->get_userinfo($_GET['userid']);
            $data['users'] = $currUser;
            echo json_encode($currUser);

        }else{
            echo "false";
            exit;
        }
  
    }


    public function login(){

        if($this->session->admindata('thar_admin_id')){
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){


            
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/login.php', $data);

        }else{

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->user_model->login($username, $password);
            $thar_admin_id = $this->admin_model->login($username, $password);

            if($thar_admin_id){
                $admin_data = array(
                    'thar_admin_id' => $thar_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);
				
                redirect('admin/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }	

        }

    }

    function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }

    public function addproject(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('project_name', 'project_name', 'required');
        if($this->form_validation->run() === FALSE){
        
		
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/addproject.php');
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $imgname = $this->do_upload();
            
            $this->admin_model->add_project($imgname);
            redirect('admin/manageproject?success');
        }
    }
   
    public function viewproject($projectid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['project'] = $this->admin_model->get_project($projectid);
        $data['questions'] = $this->admin_model->get_questions($projectid);

        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/viewproject.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function responceproject($projectid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['responces'] = $this->admin_model->get_responce($projectid);
        $data['project'] = $this->admin_model->get_project($projectid);
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/responceproject.php', $data);
        $this->load->view('templates/admin/footer.php');

    }
    public function completeproject($projectid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

      $this->admin_model->completeproject($projectid);
        redirect('admin/manageproject');
    }

    public function uncompleteproject($projectid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

      $this->admin_model->uncompleteproject($projectid);
        redirect('admin/manageproject');
    }


  
    public function manageproject(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

        $this->form_validation->set_rules('projectid', 'assignuser', 'required');
        if($this->form_validation->run() === FALSE)
        {
            $data['projects'] = $this->admin_model->get_projects();

            $this->load->view('templates/admin/header.php');
            
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/manageproject.php', $data);
            $this->load->view('templates/admin/footer.php');
        }
        else
        {
            $project_id = $this->input->post('projectid');
            $this->admin_model->assign_user($project_id);
            redirect('admin/manageproject');
            
        }
      

    }

    public function editproject($projectid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['project'] = $this->admin_model->get_project($projectid);
        $data['questions'] = $this->admin_model->get_questions($projectid);
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/editproject.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function addquestion(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

        $this->form_validation->set_rules('projectid', 'question', 'required');
        
      
        if($this->form_validation->run() === FALSE)
        {
			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/projects?fail');
        
        }
        else{
            $project_id = $this->input->post('projectid');



            $questionid = $this->admin_model->add_question($project_id);

            if($this->input->post('type') == "dropdown" || $this->input->post('type') == "dropdownvalue" || $this->input->post('type') == "options" ){
                $answer_option = $this->input->post('name');
                $data = array();
                foreach($answer_option as $val)
                {
                    $this->admin_model->add_option($val, $questionid);
                }
            }

      

		
            redirect('admin/editproject/'.$project_id);

        }

    }
    public function deleteques($project_id){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        $this->admin_model->deleteques($project_id);
        redirect('admin/editproject/'.$project_id);
    }



    public function update_project(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $this->form_validation->set_rules('project_name', 'project_name', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/projects?fail');
        }
        else{
            $imgname = $this->do_upload();

            $this->admin_model->update_project($imgname);

		
            redirect('admin/manageproject');
            
        }
  
    }
    
    public function update_survey(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('survey_name', 'survey_name', 'required');
       
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/forms?fail');
        }
        else{

            $this->admin_model->update_survey();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/managesurvey');
            
        }
  
	}

    public function del_project($project_id){

        $this->admin_model->del_project($project_id);
        redirect('admin/manageproject');
        
    }

    public function del_user($userid){

        $this->admin_model->del_user($userid);
        redirect('admin/allusers');
        
    }
    public function del_staff($staffid){

        $this->admin_model->del_staff($staffid);
        redirect('admin/administrator');
        
    }

    public function del_survey($form_id){

        $this->admin_model->del_survey($form_id);
        redirect('admin/managesurvey');
        
    }


    public function addsurvey(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->form_validation->set_rules('survey_name', 'survey_name', 'required');


        if($this->form_validation->run() === FALSE){

            $data['users'] = $this->admin_model->get_users();
		
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/addsurvey.php',$data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $this->admin_model->add_survey();
            redirect('admin/addsurvey');
        }
    }

    public function managesurvey(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        if($this->form_validation->run() === FALSE){

            $data['forms'] = $this->admin_model->get_survey();
		
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managesurvey.php',$data);
        $this->load->view('templates/admin/footer.php');
        }
        else{
            $this->admin_model->update_survey();
            redirect('admin/managesurvey');
        }
    }

    public function addmonitoring(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/addmonitoring.php');
        $this->load->view('templates/admin/footer.php');
  
    }


    public function managemonitoring(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managemonitoring.php');
        $this->load->view('templates/admin/footer.php');
  
    }

    public function pendingstock(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        
        $data['stocks'] = $this->admin_model->get_pendingstock();



        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingstock.php', $data);
        $this->load->view('templates/admin/footer.php');
  
    }


    public function activestocks(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        
        $data['stocks'] = $this->admin_model->get_activestock();



        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/activestock.php', $data);
        $this->load->view('templates/admin/footer.php');
  
    }
    

    public function expiredstocks(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        
        $data['stocks'] = $this->admin_model->get_expiredstocks();


//         echo '<pre>';
//         print_r($data);
//         echo '<pre>';
// exit;
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/expiredstock.php', $data);
        $this->load->view('templates/admin/footer.php');
  
    }


    public function activestatus($stockid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        $this->admin_model->activestatus($stockid);
        redirect('admin/activestocks');

    }


	function import()
	{
		if(isset($_FILES["file"]["name"]))
		{
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
                    $auction_date = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $auction = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $lot_number = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $maker = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $model = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$model_code = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$chassis = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$year = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $engine_cc = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $transmission = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $version_class = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $drive = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $engine_code = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $fuel = $worksheet->getCellByColumnAndRow(13, $row)->getValue();  
                    $color = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $doors = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $seats = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $dimension = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $m3 = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                    $weight = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $package = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                    $milage = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                    $condition_grade = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                    $city_location = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                    $start_price = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                    $won_price = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                    $sale_price = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                    $standard_features = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                    $reference = $worksheet->getCellByColumnAndRow(28, $row)->getValue();


					$data[] = array(
                        'auction_date'	=>	$auction_date,
                        'auction'		=>	$auction,
                        'lot_number'	=>	$lot_number,
                        'maker'		    =>	$maker,
						'model'		    =>	$model,
						'model_code'	=>	$model_code,
						'chassis'		=>	$chassis,
                        'year'		    =>	$year,
                        'engine_cc'		=>	$engine_cc,
						'transmission'	=>	$transmission,
						'version_class'	=>	$version_class,
                        'drive'		    =>	$drive,
                        'engine_code'	=>	$engine_code,
						'fuel'			=>	$fuel,
                        'color'			=>	$color,
                        'doors'			=>	$doors,
						'seats'			=>	$seats,
                        'dimension'		=>	$dimension,
                        'm3'		    =>	$m3,
						'weight'		=>	$weight,
						'package'		=>	$package,
                        'milage'		=>	$milage,
                        'condition_grade'	=>	$condition_grade,
						'city_location'		=>	$city_location,
                        'start_price'		=>	$start_price,
                        'won_price'			=>	$won_price,
						'sale_price'		=>	$sale_price,
                        'standard_features'	=>	$standard_features,
                        'reference'	        =>	$reference,
                        'status'	        =>	"pending"
					);
				}
			}
			$this->admin_model->insert($data);
            echo 'Data Imported successfully';
		}	
	}


  
 
    public function ajax_edit_staffmodal($staffid)
    {
       

        $data['staff'] = $this->admin_model->get_staffinfo($staffid);

        $this->load->view('templates/ajax/editstaff.php', $data);
    }
    

    public function ajax_edit_usermodal($userid){
        $currUser = $this->user_model->get_userinfo($userid);

        $data['user'] = $currUser;

        $this->load->view('templates/ajax/edituser.php', $data);
    }


    public function ajax_assign_user_modal($project_id){
       
        $data['project'] = $this->admin_model->get_projectinfo($project_id);
        $data['users'] = $this->admin_model->get_users();

        $this->load->view('templates/ajax/assignuser.php', $data);
    }

    public function ajax_answer_modal($servay_id){
       
        $data['servaies'] = $this->admin_model->get_servayinfo($servay_id);
      
        $this->load->view('templates/ajax/servayanswers.php', $data);
    }

    public function ajax_edit_projectsmodal($project_id){

        $data['project'] = $this->admin_model->get_projectinfo($project_id);

        $this->load->view('templates/ajax/edit_project.php', $data);
    }

    public function ajax_add_question_adminmodal($project_id){

        $data['project'] = $this->admin_model->get_projectinfo($project_id);

        $this->load->view('templates/ajax/addquestions.php', $data);
    }



    public function ajax_edit_surveymodal($form_id){

        $data['form'] = $this->admin_model->get_surveyinfo($form_id);

        $this->load->view('templates/ajax/edit_survey.php', $data);
    }


    public function logout(){

        $this->session->unset_userdata('thar_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');
		
        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }



  


    public function index(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        
        $data['projects'] = $this->admin_model->get_projects();

        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;
        
        // $data['projects'] = $this->user_model->get_assign_projects_dashboard($this->session->admindata('dashboard_thar_id'));

        // $this->form_validation->set_rules('project', 'Project ID', 'required');
        // $this->form_validation->set_rules('starttime', 'Start Time', 'required');
        // $this->form_validation->set_rules('endtime', 'End Time', 'required');


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/index.php', $data);
            $this->load->view('templates/admin/footer.php');

    

    }


    public function administrator(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

            $data['staffs'] = $this->admin_model->get_staff();
        
            $this->load->view('templates/admin/header.php');
            
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/administrator.php', $data);
            $this->load->view('templates/admin/footer.php');
           
            
    }

     public function addstaff(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
		$this->form_validation->set_rules('name', 'FullName', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('staff_error', 'something missing');
            redirect('admin/staff?fail');
        }
        else{

			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->addstaff($enc_password);
			
			$this->session->set_flashdata('staff_registered', 'You are sucessfully registered');

		
            redirect('admin/administrator');
            
        }
  
    }
    public function updatestaff(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('staff_error', 'something missing');
            redirect('admin/staffs?fail');
        }
        else{

            $this->admin_model->updatestaff();
			
			$this->session->set_flashdata('staff_registered', 'You are sucessfully registered');

		
            redirect('admin/administrator');
            
        }
  
	}


  
///////////////////////////////////////////////////**********************api**********///////////////////////
    public function getProjectsJson(){

        $data['data'] = $this->admin_model->get_projects();
        echo json_encode($data);
        // $this->load->view('templates/admin/loadjason.php', $data);
        
    }

    public function getQuestionJson($projectid)
    {
        $data['data'] = $this->admin_model->get_questions($projectid);
        echo json_encode($data);
    }
    public function getQuestionbyuserJson($userid)
    {
        $data['data'] = $this->admin_model->get_questionsbyuser($userid);
        echo json_encode($data);
    }

    public function submitedQuestion($userid)
    { if ($_GET['date'])
        {
            $data= json_decode($_GET['data']);
            $userId= $data->{'user_id'};


        }
        $data['data'] = $this->admin_model->get_questionsbyuser($userid);
        echo json_encode($data);
    }

////////////////////////////////////////////*********api***************/////////////////////////////


/////////////////////////////////////Users//////////////////////////////////////
    public function users(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/users.php');
        $this->load->view('templates/admin/footer.php');

    }

    public function pendingusers(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['users'] = $this->admin_model->get_pending_users();

        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingusers.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function activateusers(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['users'] = $this->admin_model->get_active_users();

        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/activateusers.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function allusers(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['users'] = $this->admin_model->get_users();

        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;
        
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/allusers.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

   
    public function edituser($userid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;
        $currUser = $this->admin_model->get_userinfo($userid);

        $data['user'] = $currUser;


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/edit_user.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function updateuser(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/users?fail');
        }
        else{

            $this->admin_model->updateuser();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/allusers');
            
        }
  
	}

    public function addusers(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/users?fail');
        }
        else{
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->admin_model->add_user($enc_password);
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/users');
            
        }
  
    }
    
    public function activeuser($userid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

      $this->admin_model-> activeuser($userid);
        redirect('admin/activateusers');
    }

    public function pendinguser($userid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

      $this->admin_model->pendinguser($userid);
        redirect('admin/activateusers');
    }

    public function addmonitoringuser(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
            
            $this->form_validation->set_rules('name', 'FullName', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('cnic', 'cnic', 'required');
            


            if($this->form_validation->run() === FALSE){
        
            
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/addmonitoringuser.php');
            $this->load->view('templates/admin/footer.php');

        }
        else
        {

			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->add_monitoring_user($enc_password);
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');
            redirect('admin/addmonitoringuser');
            
        }
  
    }

    public function managemonitoringuser(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['users'] = $this->admin_model->get_monitoring_users();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managemonitoringusers.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function editmonitoringuser($userid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

        $data['user'] = $this->admin_model->get_monitoring_user($userid);

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/edit_monitoring_user.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function update_monitoring_user(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/users?fail');
        }
        else{

            $this->admin_model->update_monitoring_user();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/managemonitoringuser');
            
        }
  
    }
    
   public function del_monitoring_user($userid){

        $this->admin_model->del_monitoring_user($userid);
        redirect('admin/managemonitoringuser');
    } 

    public function adddashboarduser(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
            
            $this->form_validation->set_rules('name', 'FullName', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('cnic', 'cnic', 'required');
            


            if($this->form_validation->run() === FALSE){
        
            
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/adddashboarduser.php');
            $this->load->view('templates/admin/footer.php');

        }
        else
        {

			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->add_dashboard_user($enc_password);
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');
            redirect('admin/adddashboarduser');
            
        }
  
    }

    public function managedashboarduser(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }
        
        $data['users'] = $this->admin_model->get_dashboard_users();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/managedashboarduser.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function editdashboarduser($userid){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
        }

        $data['user'] = $this->admin_model->get_dashboard_user($userid);

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/editdashboarduser.php', $data);
        $this->load->view('templates/admin/footer.php');

    }

    public function update_dashboard_user(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('cnic', 'cnic', 'required');
		


        if($this->form_validation->run() === FALSE){
       

			$this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/users?fail');
        }
        else{

            $this->admin_model->update_dashboard_user();
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

		
            redirect('admin/managedashboarduser');
            
        }
  
    }
    
    public function del_dashboard_user($userid){

        $this->admin_model->del_dashboard_user($userid);
        redirect('admin/managedashboarduser');
        
    }



/////////////////////////////////////Users End //////////////////////////////////////
    public function stocks(){
        // if(!$this->session->admindata('thar_admin_id'))
		// {
		// 	redirect('admin/login');
		// }
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/stocks.php');
        $this->load->view('templates/admin/footer.php');

    }
    

  

    public function country(){
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
        
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/country.php');
        $this->load->view('templates/admin/footer.php');

    }


 
    public function projectview($projectid)
    {
        if(!$this->session->admindata('thar_admin_id'))
		{
			redirect('admin/login');
		}
		
  
  
      //$data['projects'] = $this->admin_model->get_projects();
  
      $data['projectid'] = $projectid;
  
  
  
      $this->form_validation->set_rules('timeselection', 'Get Time', 'required');
  
  
      if ($this->form_validation->run() === FALSE) {
  
        $this->load->view('templates/users/header.php');
        $this->load->view('templates/dashboard/navbar.php');
        $this->load->view('templates/dashboard/project.php', $data);
        $this->load->view('templates/users/footer.php');
      } else {
  
  
        $str1 =  $this->input->post('timeselection');
  
        preg_match('/(.*?)-/', $str1, $m2);
        preg_match('/-(.*)/', $str1, $m3);
  
        $data['startdate'] = $m2[1] . PHP_EOL;
        $data['enddate'] = $m3[1] . PHP_EOL;
  
  
        // echo $startdate;
        // echo "<br>";
        // echo $enddate;
        // echo "<br>";
        // die;
        // echo "<hr>";
  
  
  
  
        $data['questions'] = $this->user_model->get_questions($projectid);
        $data['question_dropdowns'] = $this->user_model->get_questions_dropdown($projectid);
  
        $data['responses'] = $this->user_model->get_response($projectid);
        $data['pictures'] = $this->user_model->get_response_pictures();
  
        // foreach($data['responses'] as $response){
        //     if(strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate) ){
  
        //       echo $response['survey_response_id'];
        //       echo "<br>";
  
        //     }
        // }
        // echo "<hr>";
        // print_r($data['responses']);
        // die;
        //print_r($data['questions']);
        //print_r($data['question_dropdowns']);
        $this->load->view('templates/admin/header.php');
        
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/projectview.php', $data);
  
  
        // foreach ($data['questions'] as $question) :
        //   if ($question['question_type'] == "dropdown") {
  
        //     foreach ($data['question_dropdowns'] as $dropdown) :
        //       if ($question['question_id'] == $dropdown['question_id']) {
        //         echo "<hr>";
        //         echo "<hr>";
        //         echo $dropdown['question_dropdown_id'];
        //         echo "<br>";
        //         echo $dropdown['question_dropdown'];
        //         echo "<br>";
        //         echo "<hr>";
        //         echo "<hr>";
  
        //         foreach ($data['questions'] as $question1) :
        //           if ($question1['question_type'] != "dropdown") {
        //             echo $question1['question_id'];
        //             echo "<br> ";
        //             echo $question1['question_text'] . " = ";
  
        //             if ($question1['question_type'] == "int") {
  
        //               $response_answer_value = 0;
        //               foreach ($data['responses'] as $response) {
  
        //                 if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){
        //                   if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {
  
        //                     $reponse_answers = $this->user_model->get_survey_answer($response['survey_response_id'], $question1['question_id']);
        //                     $response_answer_value = $response_answer_value + $reponse_answers['answer_value'];
  
        //                   }
        //                 }
        //               }
  
        //               echo $response_answer_value;
        //             } elseif ($question1['question_type'] == "options") {
  
        //               echo "<hr> ";
        //               foreach ($data['question_dropdowns'] as $questiondropdown) :
        //                 if ($questiondropdown['question_id'] == $question1['question_id']) {
        //                   echo "<br> ";
        //                   echo "dropdown id" . $questiondropdown['question_dropdown_id'];
        //                   echo "<br> ";
        //                   echo "dropdown name" . $questiondropdown['question_dropdown'] . " = ";
  
        //                   $response_answer_value1 = 0;
        //                   foreach ($data['responses'] as $response) {
  
        //                     if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){
  
        //                       if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {
  
        //                         $reponse_answers1 = $this->user_model->get_survey_answer_option($response['survey_response_id'], $question1['question_id'], $questiondropdown['question_dropdown_id']);
        //                         $response_answer_value1 = $response_answer_value1 + $reponse_answers1['options_answer_value'];
  
        //                       }
        //                     }
        //                   }
  
        //                   echo $response_answer_value1;
        //                 }
        //               endforeach;
        //             } elseif ($question1['question_type'] == "image") {
  
  
                     
  
        //                 foreach ($data['responses'] as $response) :
        //                   if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){
  
        //                   if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {
  
        //                     foreach ($data['pictures'] as $picture) :
        //                       if($picture['survey_response_id'] == $response['survey_response_id']  &&  $picture['survey_response_questionid'] == $question1['question_id'] ){
        //                         echo $picture['survey_response_picture'];
        //                       }
        //                     endforeach;
            
        //                   }
        //                 }
        //                 endforeach;
  
                    
        //             }
  
  
        //             echo "<hr>";
        //           }
        //         endforeach;
        //       }
  
  
        //     endforeach;
        //   }
  
        // endforeach;
  
        $this->load->view('templates/admin/footer.php');
        //$data['responses'] = $this->user_model->get_response($startdate, $enddate, $projectid);
  
        // $data['response_answers'] = $this->user_model->get_response_answers();
  
  
        // $data['questions'] = $this->admin_model->get_questions($this->input->post('project'));
        // $data['response_answers'] = $this->user_model->get_response_answers();
  
  
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
  
        // $this->load->view('templates/users/header.php');
        // $this->load->view('templates/dashboard/navbar.php');
        // $this->load->view('templates/dashboard/project.php', $data);
        // $this->load->view('templates/users/footer.php');
  
      }
    }




}
