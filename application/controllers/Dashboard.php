<?php
class dashboard extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('user_model');
  }


  public function login()
  {
    if ($this->session->userdata('dashboard_thar_id')) {
      redirect('dashboard/');
    }

    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() === FALSE) {

      $this->session->set_flashdata('login_faileds', 'Login is invalid. Incorrect username or password.');

      redirect('users/');
    } else {
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      $user_id = $this->user_model->logindash($username, $password);
      //$employee_id = $this->employee_model->login($username, $password);

      if ($user_id) {
        $user_data = array(
          'dashboard_thar_id' => $user_id,
          'username' => $username,
          'necxo_logged_in' => true
        );
        $this->session->set_userdata($user_data);

        $this->session->set_flashdata('user_loggedins', 'You are now logged in');
        redirect('dashboard/');
      } else {
        $this->session->set_flashdata('login_faileds', 'Login is invalid. Incorrect username or password.');
        redirect('users/');
      }
    }
  }

  public function checkresponse($res, $ques, $drop){


    $data['survey_answers'] = $this->user_model->get_survey_answers($res, $ques, $drop);
    if(isset($data['survey_answers'])){
      return true;
      
    }else{
      return false;
    }

  }


  public function logout()
  {

    $this->session->unset_userdata('dashboard_thar_id');
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('necxo_logged_in');


    redirect('users/');
  }




  public function index()
  {
    if (!$this->session->admindata('dashboard_thar_id')) {
      redirect('users/login');
    }

    $data['title'] = "Monitoring Form";
    // echo $this->session->admindata('monitoring_thar_id');
    // $projects =  $this->admin_model->get_user_project($this->session->admindata('monitoring_thar_id'));

    $data['projects'] = $this->user_model->get_assign_projects_dashboard($this->session->admindata('dashboard_thar_id'));

    // if(isset($projects)){

    //     $projectid = $projects['project_id'];

    //     // echo $projectid;
    //     $data['projectid'] =  $projectid;
    //     $data['questions'] = $this->admin_model->get_questions($projectid);


    // }

    // print_r($data);

    // die;

    $this->load->view('templates/users/header.php');
    $this->load->view('templates/dashboard/navbar.php');
    $this->load->view('templates/dashboard/index.php', $data);
    $this->load->view('templates/users/footer.php');
  }

  public function projectview($projectid)
  {
    if (!$this->session->admindata('dashboard_thar_id')) {
      redirect('users/');
    }


    //$data['projects'] = $this->admin_model->get_projects();

    $data['projectid'] = $projectid;



    $this->form_validation->set_rules('timeselection', 'Get Time', 'required');


    if ($this->form_validation->run() === FALSE) {

      $this->load->view('templates/users/header.php');
      $this->load->view('templates/dashboard/navbar.php');
      $this->load->view('templates/dashboard/project.php', $data);
      $this->load->view('templates/users/footer.php');
    } else {


      $str1 =  $this->input->post('timeselection');

      preg_match('/(.*?)-/', $str1, $m2);
      preg_match('/-(.*)/', $str1, $m3);

      $data['startdate'] = $m2[1] . PHP_EOL;
      $data['enddate'] = $m3[1] . PHP_EOL;


      // echo $startdate;
      // echo "<br>";
      // echo $enddate;
      // echo "<br>";
      // die;
      // echo "<hr>";




      $data['questions'] = $this->user_model->get_questions($projectid);
      $data['question_dropdowns'] = $this->user_model->get_questions_dropdown($projectid);

      $data['responses'] = $this->user_model->get_response($projectid);
      $data['pictures'] = $this->user_model->get_response_pictures();

      // foreach($data['responses'] as $response){
      //     if(strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate) ){

      //       echo $response['survey_response_id'];
      //       echo "<br>";

      //     }
      // }
      // echo "<hr>";
      // print_r($data['responses']);
      // die;
      //print_r($data['questions']);
      //print_r($data['question_dropdowns']);
      $this->load->view('templates/users/header.php');
      $this->load->view('templates/dashboard/navbar.php');
      $this->load->view('templates/dashboard/projectview.php', $data);


      // foreach ($data['questions'] as $question) :
      //   if ($question['question_type'] == "dropdown") {

      //     foreach ($data['question_dropdowns'] as $dropdown) :
      //       if ($question['question_id'] == $dropdown['question_id']) {
      //         echo "<hr>";
      //         echo "<hr>";
      //         echo $dropdown['question_dropdown_id'];
      //         echo "<br>";
      //         echo $dropdown['question_dropdown'];
      //         echo "<br>";
      //         echo "<hr>";
      //         echo "<hr>";

      //         foreach ($data['questions'] as $question1) :
      //           if ($question1['question_type'] != "dropdown") {
      //             echo $question1['question_id'];
      //             echo "<br> ";
      //             echo $question1['question_text'] . " = ";

      //             if ($question1['question_type'] == "int") {

      //               $response_answer_value = 0;
      //               foreach ($data['responses'] as $response) {

      //                 if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){
      //                   if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {

      //                     $reponse_answers = $this->user_model->get_survey_answer($response['survey_response_id'], $question1['question_id']);
      //                     $response_answer_value = $response_answer_value + $reponse_answers['answer_value'];

      //                   }
      //                 }
      //               }

      //               echo $response_answer_value;
      //             } elseif ($question1['question_type'] == "options") {

      //               echo "<hr> ";
      //               foreach ($data['question_dropdowns'] as $questiondropdown) :
      //                 if ($questiondropdown['question_id'] == $question1['question_id']) {
      //                   echo "<br> ";
      //                   echo "dropdown id" . $questiondropdown['question_dropdown_id'];
      //                   echo "<br> ";
      //                   echo "dropdown name" . $questiondropdown['question_dropdown'] . " = ";

      //                   $response_answer_value1 = 0;
      //                   foreach ($data['responses'] as $response) {

      //                     if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){

      //                       if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {

      //                         $reponse_answers1 = $this->user_model->get_survey_answer_option($response['survey_response_id'], $question1['question_id'], $questiondropdown['question_dropdown_id']);
      //                         $response_answer_value1 = $response_answer_value1 + $reponse_answers1['options_answer_value'];

      //                       }
      //                     }
      //                   }

      //                   echo $response_answer_value1;
      //                 }
      //               endforeach;
      //             } elseif ($question1['question_type'] == "image") {


                   

      //                 foreach ($data['responses'] as $response) :
      //                   if($this->checkresponse($response['survey_response_id'], $question['question_id'], $dropdown['question_dropdown_id'])){

      //                   if (strtotime($response['time_taken']) > strtotime($startdate) && strtotime($response['time_taken']) < strtotime($enddate)) {

      //                     foreach ($data['pictures'] as $picture) :
      //                       if($picture['survey_response_id'] == $response['survey_response_id']  &&  $picture['survey_response_questionid'] == $question1['question_id'] ){
      //                         echo $picture['survey_response_picture'];
      //                       }
      //                     endforeach;
          
      //                   }
      //                 }
      //                 endforeach;

                  
      //             }


      //             echo "<hr>";
      //           }
      //         endforeach;
      //       }


      //     endforeach;
      //   }

      // endforeach;

      $this->load->view('templates/users/footer.php');
      //$data['responses'] = $this->user_model->get_response($startdate, $enddate, $projectid);

      // $data['response_answers'] = $this->user_model->get_response_answers();


      // $data['questions'] = $this->admin_model->get_questions($this->input->post('project'));
      // $data['response_answers'] = $this->user_model->get_response_answers();


      // echo '<pre>';
      // print_r($data);
      // echo '<pre>';

      // $this->load->view('templates/users/header.php');
      // $this->load->view('templates/dashboard/navbar.php');
      // $this->load->view('templates/dashboard/project.php', $data);
      // $this->load->view('templates/users/footer.php');

    }
  }
}
